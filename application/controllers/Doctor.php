<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model('dr_model');
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 3){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}

	public function index()
	{	
		redirect('Doctor/dr_list');
	}

	public function delete_dr()
	{
		$id = $_GET['id'];

		$this->db->trans_begin();

			$select = array('doc_id','(select image_url from images where ref_code = doc.doc_id AND type = "1" Order by id desc limit 1) as pro_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id asc limit 1 ) as frame_pic2');
			$where = array('doc_id'=>$id);
			$dr_data = $this->base_models->GetSingleDetails('doc', $where, $select);// get doc data

			if(!empty($dr_data->pro_pic)){
				$pro_pic_exp = explode("/","$dr_data->pro_pic"); // expload
				$pro_pic_path = FCPATH."$pro_pic_exp[4]/$pro_pic_exp[5]/$pro_pic_exp[6]";
				if(file_exists($pro_pic_path)){
					unlink($pro_pic_path); //remove profile image
				}
			}

			if(!empty($dr_data->frame_pic)){
				$frame_pic_exp = explode("/","$dr_data->frame_pic"); // expload
				$frame_pic_path = FCPATH."$frame_pic_exp[4]/$frame_pic_exp[5]/$frame_pic_exp[6]";
				if(file_exists($frame_pic_path)){
					unlink($frame_pic_path); //remove frame1 image
				}
			}

			if(!empty($dr_data->frame_pic2)){
				$frame_pic2_exp = explode("/","$dr_data->frame_pic2"); // expload
				$frame_pic2_path = FCPATH."$frame_pic2_exp[4]/$frame_pic2_exp[5]/$frame_pic2_exp[6]";
				if(file_exists($frame_pic2_path)){
					unlink($frame_pic2_path); //remove frame2 image
				}
			}

			$this->base_models->RemoveValues('images', array('type' => 1,'ref_code' => $id)); //remove profile image
			$this->base_models->RemoveValues('images', array('type' => 3,'ref_code' => $id)); //remove frames images
			$this->base_models->RemoveValues('doc', array('doc_id' => $id)); //remove doc

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}else{
			$this->db->trans_commit();
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}
		
		echo json_encode($data);
		die();
	}

	public function dr_list()
	{	
		// foreach (glob(APPPATH.'../uploads/*.zip') as $del) { // remove previous zip file
		// 	unlink($del);
		// }
		$select = array('doc_id','name','mobile_no','email','specialty','hospital','language','mr_id','mr_code','created_at','(select image_url from images where ref_code = doc.doc_id AND type = "1" Order by id desc limit 1) as pro_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id asc limit 1 ) as frame_pic2');
		$where = array('frame_status' => '1');
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Doctor/dr_list";
		$config["total_rows"] = $this->base_models->get_count('doc_id','doc', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagi_data($select,'doc', $where,'doc_id',$config["per_page"], $page);     
		//Pagination End
		
		$pagedata['filter'] = true;
		$pagedata['fdate'] = '';
		$pagedata['todate'] = '';
		$pagedata['dr_name'] = '';
		$pagedata['delete_link'] = 'Doctor/delete_dr';
		$this->renderView('Admin/Doctor/dr_list',$pagedata);
	}
	
	// with ci pagination in php
	public function dr_list_sess()
	{
		// foreach (glob(APPPATH.'../uploads/*.zip') as $del) { // remove previous zip file
		// 	unlink($del);
		// }
		// foreach (glob(APPPATH.'../uploads/export/*.zip') as $del) { // remove previous zip file
			// unlink($del);
		// }
		$fdate = date('Y-M-d');	
		$todate = date('Y-M-d');
		$dr_name = '';
		$ranges = '';
		$pagedata["links"] = '';	
		$pagedata['results'] = array();
		
		// if(!empty($this->session->userdata('fdate'))){
			
			if(!empty($_POST)){
				$ranges = explode('-',$this->input->post('daterange'));
				$fdate = date('Y-m-d', strtotime($ranges[0])).' 00:00:00';
				$todate = date('Y-m-d', strtotime($ranges[1])).' 23:59:00';
				
				$this->session->set_userdata('fdate',$fdate);
				$this->session->set_userdata('todate',$todate);
				if(!empty($_POST['dr_name'])){
					$dr_name = $_POST['dr_name'];
					$this->session->set_userdata('dr_name',$dr_name);
				}
				
				// zip file download
				// if(@$_POST['submit']=='creatzip')
				// {
					// $this->download_zip($fdate,$todate);		
					// redirect('Doctor/download_zip');
				// }
			}else{
				$fdate = $this->session->userdata('fdate');
				$todate = $this->session->userdata('todate');
				$dr_name = $this->session->userdata('dr_name');
			}

			if(@$_POST['submit']=='genexl')
			{
				$data['data'] = $this->dr_model->dr_list_sess($fdate, $todate, NULL, NULL);
				$this->generate_dr_excel($data['data']);
			}
			
			//pagination start
			$config = array();
	        $config["base_url"] = site_url() . "/Doctor/dr_list_sess";
	        $config["total_rows"] = $this->dr_model->dr_list_search($fdate, $todate, NULL, NULL, $count = true, $dr_name);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $pagedata["links"] = $this->pagination->create_links();
	        $pagedata['results'] = $this->dr_model->dr_list_search($fdate, $todate, $config["per_page"], $page, FALSE, $dr_name);
		// }
		$pagedata['filter'] = true;
		$pagedata['fdate'] = date('Y-m-d', strtotime($fdate));
		$pagedata['todate'] = date('Y-m-d', strtotime($todate));
		$pagedata['dr_name'] = $dr_name;
		$pagedata['delete_link'] = 'Doctor/delete_dr';
		$this->renderView('Admin/Doctor/dr_list',$pagedata);
	}

	public function sendMSG($msg){
		echo "data: $msg" . PHP_EOL;
		  echo PHP_EOL;
			flush();
			ob_flush();
			sleep(2);
	}
	
	
	//check current zip processing background
	public function check_zip(){
		return shell_exec("pgrep  zip");
	}
	
	//send zip in background
	public function background_zip_send(){
		$fdate = ($this->uri->segment(3)) ? date('Y-m-d', strtotime($this->uri->segment(3))).' 00:00:00' : date('Y-m-d').' 00:00:00';
        $todate = ($this->uri->segment(4)) ? date('Y-m-d', strtotime($this->uri->segment(4))).' 23:59:00' : date('Y-m-d').' 23:59:00';
		$file_zip_name = date('Y-m-d', strtotime($fdate)).'_'.date('Y-m-d', strtotime($todate)).'.zip';
		
		if($this->check_zip()==""){
			//exec('perl /var/suraj/mysqltest.pl "'.$fdate.'" "'.$todate.'" "mustaquim.sayyed@onevoice.co.in"');
			exec('perl /var/suraj/cutomefilename.pl "'.$fdate.'" "'.$todate.'" "rahil@marspoles.com" "'.$file_zip_name.'"');
			echo "You get mail after zip process done";
		}else{
			echo "Zip process is already runing please wait for mail";
		}
	}
	
public function down1_test(){
$files= array();
                $fdate = ($this->uri->segment(3)) ? date('Y-m-d', strtotime($this->uri->segment(3))).' 00:00:00' : date('Y-m-d').' 00:00:00';
                $todate = ($this->uri->segment(4)) ? date('Y-m-d', strtotime($this->uri->segment(4))).' 23:59:00' : date('Y-m-d').' 23:59:00';
                $select = array('doc_id','(select image_url from images where ref_code = doc.doc_id AND type = "3" limit 1 offset 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" limit 1 offset 0 ) as frame_pic2');
                // $select = array('doc_id','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id asc limit 1 ) as frame_pic2');
                if(!empty($fdate) && !empty($todate)){
                        $where = array('frame_status' => '1','created_at  >='=> "$fdate",'created_at  <='=> "$todate");
                }else{
                        $where = array('frame_status' => '1');

                }
                $results = $this->base_models->GetAllValues('doc', $where, $select,$orderby = '');
        $not_pending = array();
//                $this->load->library('zip');
                foreach($results as $res){
                        if($res['frame_pic'] != '' && $res['frame_pic2'] != ''){
                                $ogPath = $res['frame_pic']; // original path
                                $arr_str = explode("/","$ogPath"); // expload
                                $path = FCPATH."$arr_str[4]/$arr_str[5]/$arr_str[6]"; // path to download for zip
    //                            $this->zip->read_file($path);
//if($path!="")
if($path!="" && file_exists($path))
       array_push($files,$path);

                                $ogPath2 = $res['frame_pic2']; // original path
 	                               $arr_str2 = explode("/","$ogPath2"); // expload
                                $path2 = FCPATH."$arr_str2[4]/$arr_str2[5]/$arr_str2[6]"; // path to download for zip
  //                              $this->zip->read_file($path2);
                                // $not_pending[] = $res;
if($path2!="" && file_exists($path2))
       array_push($files,$path2);
                  }
                }
                $zipname = date('d-M', strtotime($fdate)).'_'.date('d-M', strtotime($todate)).'_frames.zip';

//print_r($files);exit();
	$totalSize = 0;
	foreach ($files as $file) {
	$totalSize += filesize($file);
	}
//echo $totalSize;
//exit();
	$totalSize += 300; //I don't understand why, but the totalSize is always 300kb short of the correct size
/*	header('Pragma: no-cache'); 
	header('Content-Description: File Download'); 
	header('Content-disposition: attachment; filename="myZip.zip"');
	header('Content-Type: application/octet-stream');
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");

	header('Content-Length: ' . $totalSize);
	header('Content-Transfer-Encoding: binary'); 
*/
	//Opening a zip stream

	$files = implode(" ", $files);
	if ($files){
		$fp = popen('zip -r -0 - ' . $files, 'r');
	}
	
//print $fp;
//exit();
	flush(); //Flushing the butter, pre streaming
	while(!feof($fp)) {
	   	echo fread($fp, 8192);
	}
	//Closing the stream
	if ($files){ 
		pclose($fp);
	}
}
        public function download_zip_test()
        {

			// foreach (glob(APPPATH.'../uploads/*.zip') as $del) { // remove previous zip file
			// 	unlink($del);
			// }

			if( empty($this->uri->segment(3)) || empty($this->uri->segment(4)) ) {
				redirect(site_url('doctor/dr_list'));
			}
header("Expires: 0");
//header('Content-Type: text/event-stream');
header('Cache-Control: no-cache'); // recommended to prevent caching of event data.
//header('Accept-Encoding: gzip, deflate');
//header('Content-Encoding: gzip');
header("Pragma: public");
//header("Expires: 0");
//header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//header("Cache-Control: public");
//header("Content-Description: File Transfer");
//header("Content-type: application/octet-stream");
//header("Content-Type: application/force-download");
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download");
//header('Content-Length: ' . 9999999*9999999);
//header("Content-Disposition: attachment; filename=\"".$filename."\"");

set_time_limit(30);

ini_set('max_execution_time', 300); //300 seconds = 5 minutes

                ini_set('memory_limit', '-1');
//header('Content-Type: application/x-gzip');
//header("Content-type: application/octet-stream");
        //$this->load->helper('download');
          //      header("Content-type: application/zip");

                $fdate = ($this->uri->segment(3)) ? date('Y-m-d', strtotime($this->uri->segment(3))).' 00:00:00' : date('Y-m-d').' 00:00:00';
                $todate = ($this->uri->segment(4)) ? date('Y-m-d', strtotime($this->uri->segment(4))).' 23:59:00' : date('Y-m-d').' 23:59:00';
                $select = array('doc_id','(select image_url from images where ref_code = doc.doc_id AND type = "3" limit 1 offset 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" limit 1 offset 0 ) as frame_pic2');
                // $select = array('doc_id','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id asc limit 1 ) as frame_pic2');
                if(!empty($fdate) && !empty($todate)){
                        $where = array('frame_status' => '1','created_at  >='=> "$fdate",'created_at  <='=> "$todate");
                }else{
                        $where = array('frame_status' => '1');

                }
                $results = $this->base_models->GetAllValues('doc', $where, $select,$orderby = '');
        $not_pending = array();
                $this->load->library('zip');
//$this->zip->archive($zipname);
                $zipname = date('d-M', strtotime($fdate)).'_'.date('d-M', strtotime($todate)).'_frames.zip';
//header("Content-Disposition: attachment; filename=\"".$zipname."\"");
//header("Pragma: public");
//header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

//$this->zip->archive(APPPATH."../uploads/".$zipname);
            //header("Content-Transfer-Encoding: binary");
            //header('Expires: 0');
            //header('Pragma: no-cache');
$zip = new ZipArchive();
if ($zip->open(APPPATH.'../uploads/'.$zipname ,ZIPARCHIVE::CREATE) === TRUE)   
              foreach($results as $key=>$res){
                        if($res['frame_pic'] != '' && $res['frame_pic2'] != ''){
                                $ogPath = $res['frame_pic']; // original path
                                $arr_str = explode("/","$ogPath"); // expload
                                $path = FCPATH."$arr_str[4]/$arr_str[5]/$arr_str[6]"; // path to download for zip
   //                             $this->zip->read_file($path);
if($path!="" && file_exists($path)){
 	// $zip->addFile($path);
 	$fileContent = file_get_contents($path);
	$zip->addFromString(basename($path), $fileContent);
}

                                $ogPath2 = $res['frame_pic2']; // original path
                                $arr_str2 = explode("/","$ogPath2"); // expload
                                $path2 = FCPATH."$arr_str2[4]/$arr_str2[5]/$arr_str2[6]"; // path to download for zip

// send it to the screen
if($path2!="" && file_exists($path2)){
	// $zip->addFile($path2);
	$fileContent2 = file_get_contents($path2);
	$zip->addFromString(basename($path2), $fileContent2);
}
                        }
                }

$zip->close() ;

header("Location: ".base_url("uploads/".$zipname));
        }


	public function download_zip()
	{
		ini_set('memory_limit', '-1');
		$fdate = ($this->uri->segment(3)) ? date('Y-m-d', strtotime($this->uri->segment(3))).' 00:00:00' : date('Y-m-d').' 00:00:00';
		$todate = ($this->uri->segment(4)) ? date('Y-m-d', strtotime($this->uri->segment(4))).' 23:59:00' : date('Y-m-d').' 23:59:00';
		$select = array('doc_id','(select image_url from images where ref_code = doc.doc_id AND type = "3" limit 1 offset 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" limit 1 offset 0 ) as frame_pic2');
		// $select = array('doc_id','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id asc limit 1 ) as frame_pic2');
		if(!empty($fdate) && !empty($todate)){
			$where = array('frame_status' => '1','created_at  >='=> "$fdate",'created_at  <='=> "$todate");
		}else{
			$where = array('frame_status' => '1');				
		}
	
		$results = $this->base_models->GetAllValues('doc', $where, $select,$orderby = '');
		$not_pending = array();
		$this->load->library('zip');			
		foreach($results as $res){
			if($res['frame_pic'] != '' && $res['frame_pic2'] != ''){
				$ogPath = $res['frame_pic']; // original path
				$arr_str = explode("/","$ogPath"); // expload
				$path = FCPATH."$arr_str[4]/$arr_str[5]/$arr_str[6]"; // path to download for zip
				$this->zip->read_file($path);
				
				$ogPath2 = $res['frame_pic2']; // original path
				$arr_str2 = explode("/","$ogPath2"); // expload
				$path2 = FCPATH."$arr_str2[4]/$arr_str2[5]/$arr_str2[6]"; // path to download for zip
				$this->zip->read_file($path2);
				// $not_pending[] = $res;
			}
		}
		$zipname = date('d-M', strtotime($fdate)).'_'.date('d-M', strtotime($todate)).'_frames.zip';
		// $this->zip->archive(FCPATH.'uploads/'.$zipname); // save on server
		$this->zip->download($zipname); // download
	}

	public function check_cmd(){
		//echo shell_exec('sh /var/suraj/test.sh');
		exec('sh /var/suraj/test.sh');
	}
	public function check_zipq(){
		echo shell_exec('sh /var/suraj/check_zip.sh');

		//$output = shell_exec('preg zip');
		//echo $output;
		//exec('sh /val/suraj/check_zip.sh "zip"',$output);
		//echo $output;
		//print_r( proc_get_status(array("preg"=>"zip")));
		//exec("preg zip", $out, $err);
		//print_r( $out);
		//exec
	}	

	public function dr_list_pending()
	{
		$select = array('doc_id','name','mobile_no','email','specialty','hospital','language','mr_id','mr_code','created_at','(select image_url from images where ref_code = doc.doc_id AND type = "1" Order by id desc limit 1) as pro_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic');
		$where = array('frame_status' => '0');
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Doctor/dr_list_pending";
		$config["total_rows"] = $this->base_models->get_count('doc_id','doc', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagi_data($select,'doc', $where,'doc_id',$config["per_page"], $page);
		
		//Pagination End
		$pagedata['filter'] = true;
		$pagedata['fdate'] = '';
		$pagedata['todate'] = '';
		$pagedata['dr_name'] = '';
		$pagedata['delete_link'] = 'Doctor/delete_dr';
		$this->renderView('Admin/Doctor/dr_list_pending',$pagedata);
	}

	public function dr_list_pending_sess()
	{
		foreach (glob(APPPATH.'../uploads/*.zip') as $del) { // remove previous zip file
			unlink($del);
		}
		$fdate = date('Y-M-d');	
		$todate = date('Y-M-d');
		$ranges = '';
		$dr_name = '';
		$pagedata["links"] = '';	
		$pagedata['results'] = array();
		
		// if(!empty($this->session->userdata('fdate'))){
			
			if(!empty($_POST)){
				$ranges = explode('-',$this->input->post('daterange'));
				$fdate = date('Y-m-d', strtotime($ranges[0])).' 00:00:00';
				$todate = date('Y-m-d', strtotime($ranges[1])).' 23:59:00';
				
				$this->session->set_userdata('fdate',$fdate);
				$this->session->set_userdata('todate',$todate);
				if(!empty($_POST['dr_name'])){
					$dr_name = $_POST['dr_name'];
					$this->session->set_userdata('dr_name',$dr_name);
				}
				
				// zip file download
				// if(@$_POST['submit']=='creatzip')
				// {
					// $this->download_zip($fdate,$todate);		
					// redirect('Doctor/download_zip');
				// }
			}else{
				$fdate = $this->session->userdata('fdate');
				$todate = $this->session->userdata('todate');
				$todate = $this->session->userdata('dr_name');
			}

			if(@$_POST['submit']=='genexl')
			{
				$data['data'] = $this->dr_model->dr_list_pending_sess($fdate, $todate);
				$this->generate_pending_dr_excel($data['data']);
			}
			
			//pagination start
			$config = array();
	        $config["base_url"] = site_url() . "/Doctor/dr_list_pending_sess";
	        $config["total_rows"] = $this->dr_model->dr_list_pending_search($fdate, $todate, NULL, NULL, $count = true, $dr_name);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $pagedata["links"] = $this->pagination->create_links();
	        $pagedata['results'] = $this->dr_model->dr_list_pending_search($fdate, $todate, $config["per_page"], $page, FALSE, $dr_name);
		// }
		$pagedata['filter'] = true;
		$pagedata['fdate'] = date('Y-m-d', strtotime($fdate));
		$pagedata['todate'] = date('Y-m-d', strtotime($todate));
		$pagedata['dr_name'] = $dr_name;
		$pagedata['delete_link'] = 'Doctor/delete_dr';
		$this->renderView('Admin/Doctor/dr_list_pending',$pagedata);
	}

	//generate to excel	
	public function generate_pending_dr_excel($param1){
		foreach (glob(APPPATH.'../uploads/admin/excel/*.xlsx') as $del) { // remove previous zip file
			unlink($del);
		}

		// create file name
		$fileName = 'PendingDocList'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'MR code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Dr Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Contact');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Specialty');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Hospital');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Language');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Date');
		
		// set Row
		$rowCount = 2;

		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['mr_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['mobile_no']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['email']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['specialty']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['hospital']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['language']);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, date('d-M-y', strtotime($element['created_at'])));			
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	
	public function generate_dr_excel($param1){
		foreach (glob(APPPATH.'../uploads/admin/excel/*.xlsx') as $del) { // remove previous zip file
			unlink($del);
		}
		// create file name
		$fileName = 'DocList'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'MR code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Dr Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Contact');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Specialty');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Hospital');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Language');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Date');
		
		// set Row
		$rowCount = 2;

		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['mr_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['mobile_no']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['email']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['specialty']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['hospital']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['language']);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, date('d-M-y', strtotime($element['created_at'])));			
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}

	public function download_zip_old()
	{	
		header("Expires: 0");
		header('Cache-Control: no-cache'); // recommended to prevent caching of event data.
		header("Pragma: public");
		set_time_limit(30);
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes
		ini_set('memory_limit', '-1');

		$fdate = ($this->uri->segment(3)) ? date('Y-m-d', strtotime($this->uri->segment(3))).' 00:00:00' : '';
		$todate = ($this->uri->segment(4)) ? date('Y-m-d', strtotime($this->uri->segment(4))).' 23:59:00' : '';
		$select = array('doc_id','(select image_url from images where ref_code = doc.doc_id AND type = "3" limit 1 offset 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" limit 1 offset 0 ) as frame_pic2');
		// $select = array('doc_id','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id asc limit 1 ) as frame_pic2');
		if(!empty($fdate) && !empty($todate)){
			$where = array('frame_status' => '1','created_at  >='=> "$fdate",'created_at  <='=> "$todate");
		}else{
			$where = array('frame_status' => '1');			
		}
	
		$results = $this->base_models->GetAllValues('doc', $where, $select,$orderby = '');
		$files = array();
		foreach($results as $res){
			if($res['frame_pic'] != '' && $res['frame_pic2'] != ''){
				$ogPath = $res['frame_pic']; // original path
				$arr_str = explode("/","$ogPath"); // expload
				$path = FCPATH."$arr_str[4]/$arr_str[5]/$arr_str[6]"; // path to download for zip
				if($path!="" && file_exists($path)){
					array_push($files,$path);
				}

				$ogPath2 = $res['frame_pic2']; // original path
				$arr_str2 = explode("/","$ogPath2"); // expload
				$path2 = FCPATH."$arr_str2[4]/$arr_str2[5]/$arr_str2[6]"; // path to download for zip
				if($path2!="" && file_exists($path2)){
					array_push($files,$path2);
				}
			}
		}
		$zipname = date('d-M', strtotime($fdate)).'_'.date('d-M', strtotime($todate)).'_frames.zip';
		$zip = new ZipArchive;
		if ($zip->open(APPPATH.'../uploads/'.$zipname ,ZIPARCHIVE::CREATE) === TRUE){
			foreach ($files as $file) {
				// download file
				$fileContent = file_get_contents($file);
				$zip->addFromString(basename($file), $fileContent);
			}
			$zip->close();
			header("Location: ".base_url("uploads/".$zipname));
		}
	}

}
