<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 2 ){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}

	}
	
	public function index(){
		$this->template_list();
	}
	
	public function template_list()
	{
		$select = array('id','image_url as image','name', 'description','(select theme_name from themes where theme_id = images.theme_id order by id limit 1) as theme_name','(select name from languages where language = code order by id limit 1) as language');
		$where = array('status' => '1','type' => '2');
				
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Template/template_list";
		$config["total_rows"] = $this->base_models->get_count('id','images', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->Base_Models->get_pagi_data($select,'images', $where,'id',$config["per_page"], $page);
		$pagedata['delete_link'] = 'Template/delete_template';
		//Pagination End
		$this->renderView('Admin/Template/template-list',$pagedata);
	}
	
	public function delete_template(){
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2'
							//'deleted_by'=>$current_date,
							//'lastUpdateUser'=>$this->session->userdata('id')
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('images',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	
	public function add()
	{
		$select = array('name','code');
		$where = array('status' => '1');
		$pagedata['languages'] = $this->Base_Models->GetAllValues ("languages",$where,$select);
		$pagedata['themes'] = $this->Base_Models->GetAllValues ("themes",$where,array('theme_id','theme_name'));
		$this->renderView('Admin/Template/add-template',$pagedata);
	}
	
	public function template_insert(){
		$this->form_validation->set_rules('language', 'Language', 'trim|required');
		$this->form_validation->set_rules('theme_id', 'Theme', 'trim|required');
		// $this->form_validation->set_rules('description', 'Description', 'trim|required');
		// $this->form_validation->set_rules('name', 'Name', 'trim|required');
		$error='';			
			if($this->form_validation->run())
			{					
				if(!empty($_FILES['image']['name'])){
					$config['upload_path'] = 'uploads/template/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$this->upload->initialize($config);
					if ($this->upload->do_upload('image')) {
						$data = $this->upload->data();
					}else{
						$this->form_validation->set_message('image', $imageerrors);	
						$imageerrors = $this->upload->display_errors();
					}
				}else{
					$data['file_name'] = '';
				}
				$path = base_url().''.$config['upload_path'].''.$data['file_name'];				
				$insert_array=array(
					'image_url'=>$path,
					'type'=>'2',
					'status'=> '1',
					'ref_code'=>'0',
					'theme_id'=>$this->input->post('theme_id'),
					'created_by'=>$this->session->userdata('user_type'),
					'language'=>$this->input->post('language'),
					// 'name'=>$this->input->post('name'),
					// 'description'=>$this->input->post('description')
				);
				
				//print_r($insert_array);exit;
				if($this->base_models->add_records('images',$insert_array)){
					$this->session->set_flashdata('success','Added successfully');
					redirect(site_url('/Template'));
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
					//redirect(base_url('admin/add_user'));
				}
			}
		//$pagedata['languages'] = array('en'=>'Engilsh','mar'=>'Marathi');
		$pagedata['languages'] = array('en'=>'Engilsh');
		$this->renderView('Admin/Template/add-template',$pagedata);
	}
	
	public function edit_template()
	{
		$id = base64_decode($_GET['id']);
		if($id==''){
			redirect(site_url('/template/template_list')); 
		}
		//get details
		$select = array('name','code');
		$where = array('status' => '1');
		$pagedata['languages'] = $this->Base_Models->GetAllValues ("languages",$where,$select);
		$pagedata['themes_list'] = $this->Base_Models->GetAllValues ("themes",$where,array('theme_id','theme_name'));
		$pagedata['themes'] = $this->Base_Models->GetSingleDetails ("images",array('id' => $id),array('id','image_url','theme_id','language','name','description'));
		$this->renderView('Admin/Template/edit-template',$pagedata);
	}
	
	public function update_template()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(site_url('template/template_list/')); 
		}
		$this->form_validation->set_rules('language', 'Language', 'trim|required');
		$this->form_validation->set_rules('theme_id', 'Theme', 'trim|required');
		// $this->form_validation->set_rules('description', 'Description', 'trim|required');
		// $this->form_validation->set_rules('name', 'Name', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
		$error='';			
		if($this->form_validation->run()){				
			if(!empty($_FILES['image']['name']))
			{
				$config1['upload_path'] = 'uploads/template/';
				$config1['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->upload->initialize($config1);
				if ($this->upload->do_upload('image'))
				{
					$data = $this->upload->data();
					//print_r($data);exit;
					$image=base_url().''.$config1['upload_path'].''.$data['file_name'];
					unlink($this->input->post('image1'));
				}
			}else{
				$image=$this->input->post('image1');
			}
			
			$update_array = array(
								'image_url'=>$image,
								'type'=>'2',
								'status'=> '1',
								'ref_code'=>'0',
								'theme_id'=>$this->input->post('theme_id'),
								'language'=>$this->input->post('language'),
								// 'name'=>$this->input->post('name'),
								// 'description'=>$this->input->post('description'),
								'updated_by'=>$current_date
								);
			
			$where_array = array('id'=>$id);
			if ($this->base_models->update_records('images',$update_array,$where_array)){
				$this->session->set_flashdata('success','Updated successfully');
			}else {
				$this->session->set_flashdata('error','Error while updating');
			}
		}
			redirect(site_url('template/edit_template/?id='.base64_encode($id))); 
		//get details
		// $select = array('name','code');
		// $where = array('status' => '1');
		// $pagedata['languages'] = $this->Base_Models->GetAllValues ("languages",$where,$select);
		// $pagedata['themes_list'] = $this->Base_Models->GetAllValues ("themes",$where,array('theme_id','theme_name'));
		// $pagedata['themes'] = $this->Base_Models->GetSingleDetails ("images",array('id' => $id),array('id','image_url','theme_id','language','name','description'));
		// $this->renderView('Admin/Template/edit-template',$pagedata);
	}
	
		
}
