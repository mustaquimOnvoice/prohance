<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class products extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		if(!empty($_POST['atsm_code']) && !empty($_POST['device_token'])){
			$this->api_model->check_token('atsm',$_POST['atsm_code'],$_POST['device_token']);
		}else{
			$response ['message'] = "fail";
			$response ['result'] =  "Param not found";
			echo json_encode($response);
			die();
		}		
    }

	//update profile ATSM
	function index(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		
		if(isset($_POST['device_token']) && isset($_POST['atsm_id'])){	
			$response ['message'] = "done";
			$response ['result'] =  "Products List";
			$select = array('id','model','tech_spec','usps','dp','mop','mrp','(select image_url from images where type = "5" AND ref_code = products.id ORDER BY id DESC limit 1) as image');
			$response['data'] = $this->Base_Models->GetAllValues ( "products",NULL,$select);
			// log_message('error', 'update : '.print_r($temp,true));
		}
		echo json_encode($response);
	}
	
	//scheme photos
	function scheme(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		
		if(isset($_POST['device_token']) && isset($_POST['atsm_id'])){	
			$response ['message'] = "done";
			$response ['result'] =  "Scheme List";
			
			$select = array('image_url as image');
			$where = array('type' => '6');
			$response['data'] = $this->Base_Models->GetAllValues ( "images",$where,$select);
			// log_message('error', 'update : '.print_r($temp,true));
		}
		echo json_encode($response);
	}
}
?>