<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class doctor extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		// if(!empty($_POST['mr_code']) && !empty($_POST['device_token'])){
			// $this->api_model->check_token('mr',$_POST['mr_code'],$_POST['device_token']);
		// }else{
			// $response ['message'] = "fail";
			// $response ['result'] =  "Param not found";
			// echo json_encode($response);
			// die();
		// }
		
    }
	
    function index(){

        echo "call";
    }
	

	function add(){
		$response ['message'] = "fail";
		$response ['result']="Param required";
		if( !empty($_POST['device_token']) && 
			!empty($_POST['mr_id'])  && 
			!empty($_POST['mr_code'])  && 
			!empty($_POST['name'])  && 
			!empty($_POST['mobile_no'])  && 
			!empty($_POST['email']) && 
			!empty($_POST['specialty']) &&
			!empty($_POST['hospital']) &&
			!empty($_POST['language'])){
				$TableValues['device_token']=$_POST['device_token'];
				$TableValues['mr_id']=$_POST['mr_id'];
				$TableValues['mr_code']=$_POST['mr_code'];
				$TableValues['name']=trim($_POST['name']);
				$TableValues['mobile_no']=trim($_POST['mobile_no']);
				$TableValues['email']=trim($_POST['email']);
				$TableValues['specialty']=trim($_POST['specialty']);
				$TableValues['hospital']=trim($_POST['hospital']);
				$TableValues['language']=trim($_POST['language']);
				$TableValues['created_at']= date('Y-m-d h:i:s');
				$TableValues['frame_status']= '0';

				$id= $this->Base_Models->AddValues ( "doc", $TableValues );
				$insert_id = $this->db->insert_id();
				$imgresponse['image_url'] = '';
				//insert photo
				foreach ($_FILES as $key => $value) {
					$imgresponse = $this->uploadImageFile($value,$insert_id ,"1",0,NULL);            
				}
				
				$response ['message'] = "done";
				$response ['result']="Successfully Uploded";
				$response ['data']=$this->input->post();					
				$response ['data']['doc_id']=$insert_id;
				$response ['data']['image']=$imgresponse['image_url'];
		}
		// log_message('error', 'img  file: '.print_r($_POST,true));
	   echo json_encode($response);
    }
	
	function profile_update(){
		$response ['message'] = "fail";
		$response ['result']="Param required";
		if( !empty($_POST['device_token']) && 
			!empty($_POST['mr_id'])  && 
			!empty($_POST['doc_id'])  && 
			!empty($_POST['mr_code'])  && 
			!empty($_POST['name'])  && 
			!empty($_POST['mobile_no'])  && 
			!empty($_POST['email']) && 
			!empty($_POST['specialty']) &&
			!empty($_POST['hospital']) &&
			!empty($_POST['language'])){
				
			$select = array('doc_id');
			$details = $this->Base_Models->GetAllValues ( "doc", array ("doc_id" => $_POST['doc_id']), $select);

			if(count($details)==1){			
				$TableValues['device_token']=$_POST['device_token'];
				$TableValues['mr_id']=$_POST['mr_id'];
				$TableValues['mr_code']=$_POST['mr_code'];
				$TableValues['name']=trim($_POST['name']);
				$TableValues['mobile_no']=trim($_POST['mobile_no']);
				$TableValues['email']=trim($_POST['email']);
				$TableValues['specialty']=trim($_POST['specialty']);
				$TableValues['hospital']=trim($_POST['hospital']);
				$TableValues['language']=trim($_POST['language']);
				$TableValues['updated_at']= date('Y-m-d h:i:s');
				$imgresponse['image_url'] = '';
				
				foreach ($_FILES as $key => $value) {
					$imgresponse = $this->uploadImageFile($value,trim($_POST['doc_id']),"1",0,NULL );
					$_POST['image_url']=$imgresponse['image_url'];
				}

				$temp = $this->Base_Models->UpadateValue ( "doc",$TableValues, array ("doc_id" => $details[0] ['doc_id'] ) );						
				$response ['message'] = "done";
				$response ['result'] = "Profile updated successfully";	

				$select = array('doc_id','mr_id','mr_code','name','mobile_no','email','specialty','hospital','language');
				$details = $this->Base_Models->GetAllValues ( "doc", array ("doc_id" => $_POST['doc_id']), $select);
				$response ['data'] = $details[0];
				$response ['data']['image']=$imgresponse['image_url'];
			}else{
				$response ['result'] = "Doctor Not exist";

			}
		}
		// log_message('error', 'img  file: '.print_r($_POST,true));
	   echo json_encode($response);
    }
	
	function add_drframe(){
		$response ['message'] = "fail";
		$response ['result']="Param required";
		if( !empty($_POST['device_token']) && 
			!empty($_POST['mr_id'])  && 
			!empty($_POST['mr_code'])  && 
			!empty($_POST['theme_id'])  && 
			!empty($_POST['language_code'])  && 
			!empty($_POST['doc_id']) ){
				$imgresponse['image_url'] = '';
				$doc_id=trim($_POST['doc_id']);
				$theme_id=trim($_POST['theme_id']);
				$language_code=trim($_POST['language_code']);
				$mr_code=trim($_POST['mr_code']);
				//get doc name
				$get_doc_name = $this->Base_Models->GetSingleDetails ("doc",array('doc_id' => $doc_id),array('name'));
				$dr_name = str_replace(' ', '_', $get_doc_name->name);
				
				//get language name
				$get_doc_name = $this->Base_Models->GetSingleDetails ("languages",array('code' => $language_code),array('name'));
				$language_name =$get_doc_name->name;
				$fileNameBefore = $mr_code.'_'.$dr_name.'_'.$language_name;
				
				//insert photo
				foreach ($_FILES as $key => $value) {
					$imgresponse = $this->uploadImageFileAddFrame($value,$doc_id ,"3",$theme_id,$language_code,$fileNameBefore);            
				}
				
				//update status
				$TableValues['frame_status']= '1';
				$this->Base_Models->UpadateValue ( "doc",$TableValues, array ("doc_id" => $doc_id ) );
				
				$response ['message'] = "done";
				$response ['result']="Successfully Uploded";
				$response ['data']=$this->input->post();
				$response ['data']['image']=$imgresponse['image_url'];
		}
		// log_message('error', 'img  file: '.print_r($_POST,true));
	   echo json_encode($response);
    }
	
	function uploadImageFileAddFrame($file,$user_id,$type=1,$theme_id=0,$language_code=NULL,$fileNameBefore) {
        $response ['message'] = "fail";
        if (isset ( $user_id) ) {
            if (isset ( $file ) && $file ['error'] == 0) {
                if (! file_exists ( APPPATH . "../uploads/" . $user_id )) {
                    mkdir ( APPPATH . "../uploads/" . $user_id, 0777, true );
                }
                    log_message('error', 'img  file: '.print_r($file,true));
                $temp = "uploads/" . $user_id ."/" .$fileNameBefore .'_'. time();
                if ($temp != "") {
                    $image_folder = APPPATH . "../" . $temp;
                    list ( $a, $b ) = explode ( '.', $file ['name'] );
                    $result = $this->imageCompress ( $file ['tmp_name'], $image_folder . "." . $b, 100 );
                    if ($result != '') {
                        $response ['message'] = "done";
                        $response ['image_url'] = base_url ( $temp . "." . $b );

                        $TableValues ['ref_code'] = $user_id;
                        $TableValues ['type'] = $type;
                        $TableValues ['image_url'] = $response ['image_url'];
                        $TableValues ['theme_id'] = $theme_id;
                        $TableValues ['language'] = $language_code;

                        $response ['upload_id'] = $this->Base_Models->AddValues ( "images", $TableValues );
                    }
                }
            }
        }
        //log_message('error', 'img : '.print_r($response,true));
        return  $response ;
    }
	
	function uploadImageFile($file,$user_id,$type=1,$theme_id=0,$language_code=NULL) {
        $response ['message'] = "fail";
        if (isset ( $user_id) ) {
            if (isset ( $file ) && $file ['error'] == 0) {
                if (! file_exists ( APPPATH . "../uploads/" . $user_id )) {
                    mkdir ( APPPATH . "../uploads/" . $user_id, 0777, true );
                }
                    log_message('error', 'img  file: '.print_r($file,true));
                $temp = "uploads/" . $user_id . "/images-" . $this->generate_random_string ( 10 );
                if ($temp != "") {
                    $image_folder = APPPATH . "../" . $temp;
                    list ( $a, $b ) = explode ( '.', $file ['name'] );
                    $result = $this->imageCompress ( $file ['tmp_name'], $image_folder . "." . $b, 100 );
                    if ($result != '') {
                        $response ['message'] = "done";
                        $response ['image_url'] = base_url ( $temp . "." . $b );

                        $TableValues ['ref_code'] = $user_id;
                        $TableValues ['type'] = $type;
                        $TableValues ['image_url'] = $response ['image_url'];
                        $TableValues ['theme_id'] = $theme_id;
                        $TableValues ['language'] = $language_code;

                        $response ['upload_id'] = $this->Base_Models->AddValues ( "images", $TableValues );
                    }
                }
            }
        }
        //log_message('error', 'img : '.print_r($response,true));
        return  $response ;
    }

    //scheme photos
	function fetch_frame(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		$this->load->model('dr_model');
		if(isset($_POST['device_token']) && isset($_POST['mr_id'])){	
			$response ['message'] = "done";
			$response ['result'] =  "Frame List";
			
			$themes = $this->Base_Models->GetAllValues ( "themes",NULL,'*');//fetch all themes			
			foreach($themes as $row){				
				$res = $this->dr_model->fetch_frame ($_POST['mr_id'],$row['theme_id']);								
				$templates[] = array(
								"name" => $row['theme_name'],
								"themes" => $res
								);
			}
			$response['data'] = $templates;
			// log_message('error', 'update : '.print_r($temp,true));
		}
		echo json_encode($response);
	}
	
	//scheme photos
	function fetch_allframe(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		$this->load->model('dr_model');
		if(isset($_POST['device_token']) && isset($_POST['mr_id'])){	
			$response ['message'] = "done";
			$response ['result'] =  "Frame List";
			
			$select = array('doc_id','name','mobile_no','email','specialty','hospital');
			$docs = $this->Base_Models->GetAllValues ( "doc",array('mr_id'=>trim($_POST['mr_id']),'frame_status' => '1'),$select);//fetch all docs
			$templates = array();
			foreach($docs as $row){				
				$res = $this->dr_model->fetch_allframe($row['doc_id']);	
				//echo $res;
				//echo $this->db->last_query();
				// $languages = array_column($res,'language');
				$templates[] = array(
								"doc_id" => $row['doc_id'],
								"name" => $row['name'],
								"mobile_no" => $row['mobile_no'],
								"email" => $row['email'],
								"specialty" => $row['specialty'],
								"hospital" => $row['hospital'],
								"ref_code" => $res[0]['ref_code'],
								"theme_id" => $res[0]['theme_id'],
								"theme_name" => $res[0]['theme_name'],
								// "themess" => $res,
								"themes" => $this->explode_dr($res)
								);
				
				//$templates[]['theme_name'] = 'd';
				// "themes" => array(
					// 'language' => explode(',',$res['language'])
					// )
			}
			$response['data'] = $templates;
		}
		echo json_encode($response);
	}
	
	function explode_dr($res){	
		$this->load->helper('common_helper');
		//print_r($res);
		//die;

		foreach($res as $row){				
			// $rows[] =  $this->explode_string($row['language'],$row['image_url'],$row['imagename'],$row['description']);
			$rows = explode(',',$row['language']);



			$image_url = explode(',',$row['image_url']);
			if(empty($row['imagename'])){
				$imagename = array('0' => '', '1' => '');
			}else{
				$imagename = explode(',',$row['imagename']);				
			}




			if(empty($row['image_url'])){
				$imageurl = array('0' => '', '1' => '');
			}else{
				$imageurl = explode(',',$row['image_url']);				
			}





			if(empty($row['description'])){
				$description = array('0' => '', '1' => '');
			}else{
				$description = explode(',',$row['description']);
			}
			
			$result = array();	
			
			//error_reporting(0);
			$x = 0;
			foreach($rows as $key => $value){
$imagename="";
				if($imagename != ""){
					$imagename=$imagename;
				}
				$fetchimageurl="";
				if($imageurl[$x]){
					$fetchimageurl=$imageurl[$x];
				}
				$description="";
				if($description){
					$description=$description;
				}
				$result[] = array('language'=>getlanguageName($value),'image_url' =>$fetchimageurl,'imagename' =>$imagename,'description' =>$description);
				$x++;
			}
		}	
		//print_r($row);
		//	die;	
		return $result;
	}
	
	function explode_string($row,$image_url,$imagename,$description){
		$this->load->helper('common_helper');
		// if(empty($row)){
			// $rows = array('0' => '', '1' => '');
			$rows = explode(',',$row);
			$image_url = explode(',',$image_url);
			if(empty($imagename)){
				$imagename = array('0' => '', '1' => '');
			}else{
				$imagename = explode(',',$imagename);				
			}
			if(empty($description)){
				$description = array('0' => '', '1' => '');
			}else{
				$description = explode(',',$description);
			}
			
			$res = array();	
			$x = 0;
			foreach($rows as $key => $value){
				$res[] = array('language'=>getlanguageName($value),'image_url' =>$image_url[$x],'imagename' =>$imagename[$x],'description' =>$description[$x]);
				$x++;
			}
		// }else{
			// $rows = explode(',',$row);
		// }
		return $res;
	}
	
	function fetch_allframe_old(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		$this->load->model('dr_model');
		if(isset($_POST['device_token']) && isset($_POST['mr_id'])){	
			$response ['message'] = "done";
			$response ['result'] =  "Frame List";
			
			$select = array('doc_id','name','mobile_no','email','specialty','hospital','language');
			$docs = $this->Base_Models->GetAllValues ( "doc",array('mr_id'=>trim($_POST['mr_id'])),$select);//fetch all themes			
			foreach($docs as $row){	
				$doctors[] = array(
							'doctor' => $row,
							'themes' => array()
							);
				// $doctors['doctor'][] = array('the'=> 's');
			}
			$response['data'] = $doctors;//fetch all themes		
			// log_message('error', 'update : '.print_r($temp,true));
		}
		echo json_encode($response);
	}

	//Fetch Dr. Empty frame images 
	function fetch_empty_frame(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		$this->load->model('dr_model');
		if(isset($_POST['device_token']) && isset($_POST['mr_id'])){	
			$response ['message'] = "done";
			$response ['result'] =  "Frame List";
			$arr = $this->dr_model->fetch_empty_frame ($_POST['mr_id']);				
				$x = 0;
				foreach ($arr as $row) {
					if($row['dr_image'] == null){
						$arr[$x]['dr_image'] = "";
					}
					$x++;
				}
			$response ['data'] = array_values($arr);
			// log_message('error', 'update : '.print_r($temp,true));
		}
		echo json_encode($response);
	}

	public function delete_dr()
	{
		$response ['status'] = "fail";
		$response ['result'] =  "Param required";

		if(!empty($_POST['dr_id']) && !empty($_POST['device_token']) && !empty($_POST['mr_id'])){

			$id = $_POST['dr_id'];			
			$mr_id = $_POST['mr_id'];			
			$this->db->trans_begin();

			$select = array('doc_id','(select image_url from images where ref_code = doc.doc_id AND type = "1" Order by id desc limit 1) as pro_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id asc limit 1 ) as frame_pic2');
			$where = array('doc_id'=>$id,'mr_id'=>$mr_id);
			$dr_data = $this->base_models->GetSingleDetails('doc', $where, $select);// get doc data
			
			if(!empty($dr_data)){
				if(!empty($dr_data->pro_pic)){
					$pro_pic_exp = explode("/","$dr_data->pro_pic"); // expload
					$pro_pic_path = FCPATH."$pro_pic_exp[4]/$pro_pic_exp[5]/$pro_pic_exp[6]";
					if(file_exists($pro_pic_path)){
						unlink($pro_pic_path); //remove profile image
					}
				}

				if(!empty($dr_data->frame_pic)){
					$frame_pic_exp = explode("/","$dr_data->frame_pic"); // expload
					$frame_pic_path = FCPATH."$frame_pic_exp[4]/$frame_pic_exp[5]/$frame_pic_exp[6]";
					if(file_exists($frame_pic_path)){
						unlink($frame_pic_path); //remove frame1 image
					}
				}

				if(!empty($dr_data->frame_pic2)){
					$frame_pic2_exp = explode("/","$dr_data->frame_pic2"); // expload
					$frame_pic2_path = FCPATH."$frame_pic2_exp[4]/$frame_pic2_exp[5]/$frame_pic2_exp[6]";
					if(file_exists($frame_pic2_path)){
						unlink($frame_pic2_path); //remove frame2 image
					}
				}

				$this->base_models->RemoveValues('images', array('type' => 1,'ref_code' => $id)); //remove profile image
				$this->base_models->RemoveValues('images', array('type' => 3,'ref_code' => $id)); //remove frames images
				$this->base_models->RemoveValues('doc', array('doc_id' => $id)); //remove doc

				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					$response['status'] = 'fail';
					$response['result'] = 'Somting went worng please try again';
				}else{
					$this->db->trans_commit();
					$response['status'] = 'done';
					$response['result'] = 'Successfully deleted';
				}
			}else{
				$response['status'] = 'fail';
				$response['result'] = 'Doctor not found or is not under you';
			}
		}
		echo json_encode($response);
	}
}
?>