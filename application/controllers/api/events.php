<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class events extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		if(!empty($_POST['user_type']) && $_POST['user_type'] == 'district_admin' ){
			$this->api_model->check_token('mpyc_district_admin',$_POST['user_mobile_no'],$_POST['device_token']);
		}else if(!empty($_POST['user_type']) && $_POST['user_type'] == 'user' ){
			$this->api_model->check_token('mpyc_users',$_POST['user_mobile_no'],$_POST['device_token']);
		}else{
			$response ['message'] = "fail";
			$response ['result'] =  "User type not found";
			echo json_encode($response);
			die();
		}
		
    }
    /**
	 * new noc application 
	 *
	 * @param
	 *        	Parameters as
	 *        	    user_mobile_no
     *     mandal_name
     *     area 
     *     road
     *     depth
     *     width
     * 
	 * @return message string and result
	 */
    function index(){

        echo "call";
    }
	

	function event_list(){

	   $response ['message'] = "fail";
		$response ['result'] =  "Unable to access";
		$event_list=null;

		if(isset($_POST['user_mobile_no']) && isset($_POST['device_token'])){
		  if(isset($_POST['event_id'])){
				$event_list= $this->Base_Models->GetAllValues ( "events" ,array('id' => $_POST['event_id'] ));
				foreach ($event_list as $key => $value) {
					$event_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"1")  );
					$event_list[$key]["images"]=json_encode($event_images);
				}
				$response ['message'] = "done";
				$response ['result'] =  "Event List";
		  }else
				$event_list= $this->Base_Models->GetAllValues ( "events" ,array("user_mobile_no"=>$_POST['user_mobile_no']) );

				foreach ($event_list as $key => $value) {
						$event_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"1")  );
						$event_list[$key]["images"]=json_encode($event_images);
					}    

				$response ['message'] = "done";
				$response ['result'] =  "Event List";

				$response ['events'] =  $event_list;

		}

		echo json_encode($response);
	}

	function district_event_list(){
	   $response ['message'] = "fail";
		$response ['result'] =  "Unable to access";
		$event_list=null;

		if(isset($_POST['user_district']) && isset($_POST['user_mobile_no']) && isset($_POST['device_token'])){
			
		// $this->api_model->check_token('mpyc_district_admin',$_POST['user_mobile_no'],$_POST['device_token']);
		
		  if(isset($_POST['event_id'])){
				$event_list= $this->Base_Models->GetAllValues ( "events" ,array('id' => $_POST['event_id'] ));
				foreach ($event_list as $key => $value) {
					$event_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"1")  );
					$event_list[$key]["images"]=json_encode($event_images);
				}
				$response ['message'] = "done";
				$response ['result'] =  "Event List";
		  }else{
				$sTerms = explode(',', $_POST['user_district']);
				$sTermBits = array();
				foreach ($sTerms as $term1) {
					$term1 = trim($term1);
					if (!empty($term1)) {
						$sTermBits[] = "user_district = '$term1'";
					}
				}
				$mobile_numbs = $this->Base_Models->CustomeQuary("SELECT user_mobile_no FROM mpyc_users WHERE ".implode(' OR ', $sTermBits));
								
				// $searchTerms = explode(',', $_POST['user_district']);
				$searchTerms = $mobile_numbs;
				$searchTermBits = array();
				foreach ($searchTerms as $term) {
					$term = trim($term['user_mobile_no']);
					if (!empty($term)) {
						$searchTermBits[] = "user_mobile_no = $term";
					}
				}
				
				//pagination
					$r = $this->Base_Models->CustomeQuary("SELECT COUNT('id') as cnt FROM events WHERE ".implode(' OR ', $searchTermBits));
	
					$numrows = $r[0]['cnt'];
					// number of rows to show per page
					$rowsperpage = 10;
					 
					// find out total pages
					$totalpages = ceil($numrows / $rowsperpage);
					 
					// get the current page or set a default
					if (isset($_POST['currentpage']) && is_numeric($_POST['currentpage'])) {
						$currentpage = (int) $_POST['currentpage'];
					} else {
						$currentpage = 1;  // default page number
					}
					 
					// if current page is less than first page
					if ($currentpage < 1) {
						// set current page to first page
						$currentpage = 1;
					}
					 
					// the offset of the list, based on current page
					$offset = ($currentpage - 1) * $rowsperpage;
				//pagination end
				
					// if current page is greater than total pages
					if ($currentpage > $totalpages) {
						// set current page to last page
						// $currentpage = $totalpages;
						$event_list = array();
					}else{
						$event_list = $this->Base_Models->CustomeQuary("SELECT * FROM events WHERE ".implode(' OR ', $searchTermBits). " ORDER BY id DESC LIMIT $offset, $rowsperpage");
										
						foreach ($event_list as $key => $value) {
								$event_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"1")  );
								$event_list[$key]["images"]=json_encode($event_images);
							}
					}
				$response ['message'] = "done";
				$response ['result'] =  "Event List";

		  }
				$response ['events'] =  $event_list;
		}

		echo json_encode($response);
	}

    function host_event(){
		$response ['message'] = "fail";
		$response ['result']="";
		if( isset($_POST['device_token']) && 
			isset($_POST['title'])  && 
			isset($_POST['desciption'])  && 
			isset($_POST['location'])  && 
			isset($_POST['start_date']) && 
			isset($_POST['end_date']) &&
			isset($_POST['user_mobile_no'])         
		){
			$TableValues['title']=$_POST['title'];
			$TableValues['desciption']=$_POST['desciption'];
			$TableValues['location']=$_POST['location'];
			$TableValues['start_date']=$_POST['start_date'];
			$TableValues['end_date']=$_POST['end_date'];
			$TableValues['user_mobile_no']=$_POST['user_mobile_no'];
			$TableValues['device_token']=$_POST['device_token'];

			$id= $this->Base_Models->AddValues ( "events", $TableValues );
			foreach ($_FILES as $key => $value) {
				$imgresponse = $this->uploadImageFile($value,$id );            
			}
			
			//get end user details
			$usercondition = array('user_mobile_no'=>$_POST['user_mobile_no']);
			$user_details = $this->Base_Models->GetAllValues('mpyc_users', $usercondition, array('user_district'));
			$user_district = $user_details[0]['user_district'];
			
			//search districtadmin from district
			$admindetails = $this->Base_Models->CustomeQuary("SELECT ad.user_id as admin_id, ad.device_token as device_token, ad.user_mobile_no FROM mpyc_districts as d LEFT JOIN mpyc_district_admin as ad ON ad.user_id = d.districtAdminId WHERE d.districtName = '$user_district'");
			
			$message = "Event: ".$_POST['title']." has been hosted ";
				$cnt=count($admindetails);
				for($x=0;$x<$cnt;$x++){
					//send firebase notification
					if(!empty($admindetails[$x]['device_token'])){
						$this->pushNotification($admindetails[$x]['device_token'], $message, $admindetails[$x]['admin_id'], '1', "Event", $id);
					}
					
					//send sms to mobile
					//$this->message_send ( $message,  $admindetails[$x]['user_mobile_no']);
				}

			$response ['message'] = "done";
			$response ['result']="Event hosted successfully";
		}

		// log_message('error', 'img  file: '.print_r($_POST,true));
		   echo json_encode($response);
    }
	
	function rate_event(){
		$response ['message'] = "fail";
		$response ['result'] =  "Unable to access";
		$meeting_list=null;

		if(isset($_POST['event_id']) && isset($_POST['user_id']) && isset($_POST['user_mobile_no']) && isset($_POST['device_token']) && isset($_POST['rating'])){
			
			// $this->api_model->check_token('mpyc_district_admin',$_POST['user_mobile_no'],$_POST['device_token']);
			
			//update rating
			$data['rating'] = $_POST['rating'];
			$wherecondition = array('id'=>$_POST['event_id']);
			$this->Base_Models->UpadateValue('events', $data, $wherecondition);
			
			//get event details
			$event_details = $this->Base_Models->GetAllValues('events', $wherecondition, array('id','title','user_mobile_no'));
			$title = $event_details[0]['title'];
			
			//get end user details
			$usercondition = array('user_mobile_no'=>$event_details[0]['user_mobile_no']);
			$user_details = $this->Base_Models->GetAllValues('mpyc_users', $usercondition, array('device_token'));
			$user_device_token = $user_details[0]['device_token'];
			
			//send firebase notification
			$district_admin_id = $_POST['user_id'];
			$message = "Your Event: ".$title." has been rated ".$_POST['rating'];
			$this->pushNotification($user_device_token, $message, $district_admin_id, '0', "Event", $event_details[0]['id']);
			
			//send sms to mobile
			$this->message_send ( $message,  $event_details[0]['user_mobile_no']);
			
			$response ['message'] = "done";
			$response ['result'] =  "Rating successful";
		}
		echo json_encode($response);
	}


	function uploadImageFile($file,$user_id,$type=1) {
        $response ['message'] = "fail";
        if (isset ( $user_id) ) {
            if (isset ( $file ) && $file ['error'] == 0) {
                if (! file_exists ( APPPATH . "../uploads/" . $user_id )) {
                    mkdir ( APPPATH . "../uploads/" . $user_id, 0777, true );
                }
                    log_message('error', 'img  file: '.print_r($file,true));
                $temp = "uploads/" . $user_id . "/images_unitglo_mobile-" . $this->generate_random_string ( 10 );
                if ($temp != "") {
                    $image_folder = APPPATH . "../" . $temp;
                    list ( $a, $b ) = explode ( '.', $file ['name'] );
                    $result = $this->imageCompress ( $file ['tmp_name'], $image_folder . "." . $b, 80 );
                    if ($result != '') {
                        $response ['message'] = "done";
                        $response ['image_url'] = base_url ( $temp . "." . $b );

                        $TableValues ['ref_id'] = $user_id;
                        $TableValues ['type'] = $type;
                        $TableValues ['image_url'] = $response ['image_url'];

                        $response ['upload_id'] = $this->Base_Models->AddValues ( "images", $TableValues );
                    }
                }
            }
        }
        //log_message('error', 'img : '.print_r($response,true));
        return  $response ;
    }
	
	function admin_notification_list(){

	   $response ['message'] = "fail";
		$response ['result'] =  "Unable to access";
		$notification_list=null;

		if(isset($_POST['user_mobile_no']) && isset($_POST['device_token']) && isset($_POST['user_id'])){
			
			// if($_POST['type'] == 'Event'){
				// $type = 1;
			// }else if($_POST['type'] == 'Meeting'){
				// $type = 3;
			// }
			
			//pagination
					$r = $this->Base_Models->CustomeQuary("SELECT count(notification_id) as cnt FROM user_notification where district_admin_id = ".$_POST['user_id']." AND type = 1 OR type = 3");
	
					$numrows = $r[0]['cnt'];
					// number of rows to show per page
					$rowsperpage = 10;
					 
					// find out total pages
					$totalpages = ceil($numrows / $rowsperpage);
					 
					// get the current page or set a default
					if (isset($_POST['currentpage']) && is_numeric($_POST['currentpage'])) {
						$currentpage = (int) $_POST['currentpage'];
					} else {
						$currentpage = 1;  // default page number
					}
					 
					// if current page is less than first page
					if ($currentpage < 1) {
						// set current page to first page
						$currentpage = 1;
					}
					 
					// the offset of the list, based on current page
					$offset = ($currentpage - 1) * $rowsperpage;
				//pagination end
				
					// if current page is greater than total pages
					if ($currentpage > $totalpages) {
						$event_list = array();
					}else{
						$notification_list= $this->Base_Models->CustomeQuary ( "SELECT * FROM user_notification where district_admin_id = ".$_POST['user_id']." AND type = 1 OR type = 3 ORDER BY notification_id DESC LIMIT $offset, $rowsperpage" );
					}
			

			$response ['message'] = "done";
			$response ['result'] =  "Notification List";

			$response ['notifications'] =  $notification_list;

		}

		echo json_encode($response);
	}

	//-- Meeting --//
	function host_meeting(){
		$response ['message'] = "fail";
		$response ['result']="fail";
		if( isset($_POST['device_token']) && 
			isset($_POST['title'])  && 
			isset($_POST['date'])  && 
			isset($_POST['location'])  && 
			isset($_POST['mom']) && 
			isset($_POST['next_meeting']) &&
			isset($_POST['attendee']) &&
			isset($_POST['user_mobile_no'])
		){
			$TableValues['title']=$_POST['title'];
			$TableValues['date']=$_POST['date'];
			$TableValues['location']=$_POST['location'];
			$TableValues['mom']=$_POST['mom'];
			$TableValues['attendee']=$_POST['attendee'];

			$TableValues['next_meeting']=$_POST['next_meeting'];
			$TableValues['user_mobile_no']=$_POST['user_mobile_no'];
			$TableValues['device_token']=$_POST['device_token'];

			$id= $this->Base_Models->AddValues ( "meetings", $TableValues );
			foreach ($_FILES as $key => $value) {
				$imgresponse = $this->uploadImageFile($value,$id ,"2");            
			}
			
			//get end user details
			$usercondition = array('user_mobile_no'=>$_POST['user_mobile_no']);
			$user_details = $this->Base_Models->GetAllValues('mpyc_users', $usercondition, array('user_district'));
			$user_district = $user_details[0]['user_district'];
			
			//search districtadmin from district
			$admindetails = $this->Base_Models->CustomeQuary("SELECT ad.user_id as admin_id, ad.device_token as device_token, ad.user_mobile_no FROM mpyc_districts as d LEFT JOIN mpyc_district_admin as ad ON ad.user_id = d.districtAdminId WHERE d.districtName = '$user_district'");
			
			$message = "Meeting: ".$_POST['title']." has been hosted ";
				$cnt=count($admindetails);
				for($x=0;$x<$cnt;$x++){
					//send firebase notification
					if(!empty($admindetails[$x]['device_token'])){
						$this->pushNotification($admindetails[$x]['device_token'], $message, $admindetails[$x]['admin_id'], '3', "Meeting", $id);
					}
					
					//send sms to mobile
					//$this->message_send ( $message,  $admindetails[$x]['user_mobile_no']);
				}


			$response ['message'] = "done";
			$response ['result']="";
		}

		// log_message('error', 'img  file: '.print_r($_POST,true));
		   echo json_encode($response);
	}
	
	function last_host_meeting(){
		$response ['message'] = "fail";
		$response ['result']="";
		if( isset($_POST['device_token']) && 
			isset($_POST['user_mobile_no'])
		){
			 $meetings= $this->Base_Models->CustomeQuary ( "select *,next_meeting as nextmeeting from meetings where user_mobile_no='".$_POST['user_mobile_no']."' order by id desc limit 1 " );

			foreach ($meetings as $key => $value) {
					$meetings_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"2")  );
					$meetings[$key]["images"]=json_encode($meetings_images);
				}    

			$response ['message'] = "done";
			$response ['result'] =  "Meeting List";

			$response ['lastmeetings'] =  $meetings;

		}
		echo json_encode($response);
	}

	function host_meeting_list(){
		$response ['message'] = "fail";
		$response ['result']="";
		if( isset($_POST['device_token']) && 
			isset($_POST['user_mobile_no'])
		){
			 $meetings= $this->Base_Models->CustomeQuary ( "select  *,next_meeting as nextmeeting from meetings where user_mobile_no='".$_POST['user_mobile_no']."' order by id desc" );

			foreach ($meetings as $key => $value) {
					$meetings_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"2")  );
					$meetings[$key]["images"]=json_encode($meetings_images);
				}    

			$response ['message'] = "done";
			$response ['result'] =  "Meeting List";

			$response ['meetinglist'] =  $meetings;

		}
		echo json_encode($response);
	}
	
	function district_meeting_list(){
	   $response ['message'] = "fail";
		$response ['result'] =  "Unable to access";
		$meeting_list=null;

		if(isset($_POST['user_district']) && isset($_POST['user_mobile_no']) && isset($_POST['device_token'])){
			
		// $this->api_model->check_token('mpyc_district_admin',$_POST['user_mobile_no'],$_POST['device_token']);
		
		  if(isset($_POST['meeting_id'])){
				$meeting_list= $this->Base_Models->GetAllValues ( "meetings" ,array("id"=>$_POST['meeting_id']));
				foreach ($meeting_list as $key => $value) {
					$meeting_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"2")  );
					$meeting_list[$key]["images"]=json_encode($meeting_images);
				}
				$response ['message'] = "done";
				$response ['result'] =  "Meeting List";

		  }else{
				$sTerms = explode(',', $_POST['user_district']);
				$sTermBits = array();
				foreach ($sTerms as $term1) {
					$term1 = trim($term1);
					if (!empty($term1)) {
						$sTermBits[] = "user_district = '$term1'";
					}
				}
				$mobile_numbs = $this->Base_Models->CustomeQuary("SELECT user_mobile_no FROM mpyc_users WHERE ".implode(' OR ', $sTermBits));
				// $searchTerms = explode(',', $_POST['user_district']);
				$searchTerms = $mobile_numbs;
				$searchTermBits = array();
				foreach ($searchTerms as $term) {
					$term = trim($term['user_mobile_no']);
					if (!empty($term)) {
						$searchTermBits[] = "user_mobile_no = $term";
					}
				}
				
				//pagination
					$r = $this->Base_Models->CustomeQuary("SELECT COUNT('id') as cnt FROM meetings WHERE ".implode(' OR ', $searchTermBits));
				
					$numrows = $r[0]['cnt'];
					// number of rows to show per page
					$rowsperpage = 10;
					 
					// find out total pages
					$totalpages = ceil($numrows / $rowsperpage);
					 
					// get the current page or set a default
					if (isset($_POST['currentpage']) && is_numeric($_POST['currentpage'])) {
						$currentpage = (int) $_POST['currentpage'];
					} else {
						$currentpage = 1;  // default page number
					}
					 
					// if current page is less than first page
					if ($currentpage < 1) {
						// set current page to first page
						$currentpage = 1;
					}
					 
					// the offset of the list, based on current page
					$offset = ($currentpage - 1) * $rowsperpage;
				//pagination end
				
					// if current page is greater than total pages
					if ($currentpage > $totalpages) {
						// set current page to last page
						// $currentpage = $totalpages;
						$meeting_list = array();
					}else{
						$meeting_list = $this->Base_Models->CustomeQuary("SELECT * FROM meetings WHERE ".implode(' OR ', $searchTermBits). " ORDER BY id DESC LIMIT $offset, $rowsperpage");
										
						foreach ($meeting_list as $key => $value) {
								$event_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"2")  );
								$meeting_list[$key]["images"]=json_encode($event_images);
							}
					}

				$response ['message'] = "done";
				$response ['result'] =  "Meeting List";
		  }
				$response ['meetings'] =  $meeting_list;
		}

		echo json_encode($response);
	}

	function test(){
		$data_return = $this->pushNotification ("eskdYxYFwic:APA91bGmM8qncCe9V1_4_dmmsAIe6fkfnO3xmnhVrR1u3ppQ2kra2IJSqNC1sEXnk8PZa9GQWQ672ljpL3e41buOPgHmPgVrh0HIC8_JKb4O1zek5Wr1OYcuiWRjwyaVigCQwYclj7xp", "test", 'Test 1 ' );
		print_r($data_return);
	}
}
?>