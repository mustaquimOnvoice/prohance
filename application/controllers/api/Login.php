<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class login extends Base_Controller {
	public function __construct() {
		parent::__construct ();
	}
	
    public function status() {
        $response ['message'] = "done";
        $response ['version'] =  "1.0";
		$response ['flag'] =  TRUE;
        // $response ['maintenance_mode'] =  false;
        // $response ['data'] =  $this->Base_Models->GetSingleDetails('tbl_status', array('status' => '1') , array('title','description','(select image_url from images where type = "4" AND ref_code = tbl_status.id ORDER BY id DESC limit 1) as image'));
        echo json_encode($response);
    }
	
    public function index() {
		echo 'call';
	}
	
	//Login ASM/TSM
	public function step1() {
		$response ['message'] = "fail";
		$response ['result'] =  "Param is reqired";
		if( isset($_POST['mr_code']) && isset($_POST['device_token']) ){
			$mr_code = trim($_POST['mr_code']);
			
			$select = array('mr_id');
			$where = array('mr_code'=>$mr_code, 'user_status'=>'2');
			$check = $this->Base_Models->GetAllValues ( "mr" ,$where, $select);//check
			if(count($check)== 0){
				$response ['result'] = 'MR code not found';
			}else{
				$response ['message'] = "done";
				$response ['result'] = 'MR code found';
			}
		}
		echo json_encode ($response);
	}
	
	public function step2() {
		$response ['message'] = "fail";
		$response ['result'] =  "Param is reqired";
		if( isset($_POST['mr_code']) && isset($_POST['mobile_no']) && isset($_POST['email']) && isset($_POST['device_token']) ){
			$mr_code = trim($_POST['mr_code']);
			$mobile_no = trim($_POST['mobile_no']);
			$email = trim($_POST['email']);
			
			//check code exist
			$select1 = array('mr_id');
			$where1 = array('mr_code'=>$mr_code, 'user_status'=>'2');
			$check1 = $this->Base_Models->GetAllValues ( "mr" ,$where1, $select1);//check
			if(count($check1)== 0){
				$response ['result'] = 'MR code not found';
			}else{			
				$select = array('mr_id','mobile_no','mr_code','fname','email','hq','state','division','designation');
				$where = array('mr_code'=>$mr_code,'mobile_no'=>$mobile_no, 'email'=>$email, 'user_status'=>'2');
				$check = $this->Base_Models->GetAllValues ( "mr" ,$where, $select);//check
				if(count($check)== 0){
					$response ['result'] = 'Mobile No. Email not match with this code: '.$mr_code;
				}else{
					$update = array ("device_token"=>$_POST['device_token']);
					$this->Base_Models->UpadateValue ( "mr",$update, $where );
					
					$response ['message'] = "done";
					$response ['result'] = 'Matched';
					$response ['data'] = $check;
				}
			}
		}
		echo json_encode ($response);
	}
}
?>