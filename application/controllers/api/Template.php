<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class template extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		// if(!empty($_POST['mr_code']) && !empty($_POST['device_token'])){
			// $this->api_model->check_token('mr',$_POST['mr_code'],$_POST['device_token']);
		// }else{
			// $response ['message'] = "fail";
			// $response ['result'] =  "Param not found";
			// echo json_encode($response);
			// die();
		// }		
    }
	
	//scheme photos
	function fetch_all(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		
		if(isset($_POST['device_token']) && isset($_POST['mr_id']) && isset($_POST['language'])){	
			$response ['message'] = "done";
			$response ['result'] =  "Template List";
			
			//fetch only English - default
			$select = array('image_url as image','name','language as language_code','(select name from languages where language = languages.code ORDER BY id limit 1) as language',"IFNULL(description,'') as description",'theme_id');
			$where = array('status' => '1');
			$themes = $this->Base_Models->GetAllValues ( "themes",$where,'*');//fetch all themes
			
			if(trim($_POST['language']) != 'eng'){//if not english
				//search other language also
				$other_lang = trim($_POST['language']);
				foreach($themes as $row){
					$where = array('type' => '2','status' => '1','theme_id' => $row['theme_id']);
					$where_in = array("eng","$other_lang");
					$res = $this->db->select($select)
									->from('images')
									->where($where)
									->where_in("language",$where_in)
									->get()->result_array();
					
					if(!empty($res)){
						$templates[] = array(
										"name" => $row['theme_name'],
										"themes" => $res
										);
					}
				}				
			}else{
				foreach($themes as $row){
					$where = array('type' => '2','status' => '1','theme_id' => $row['theme_id'],'language' => 'eng');
					$res = $this->db->select($select)
									->from('images')
									->where($where)
									->get()->result_array();
					if(!empty($res)){
						$templates[] = array(
										"name" => $row['theme_name'],
										"themes" => $res
										);
					}
				}
			}
			$response['data'] = $templates;
			// log_message('error', 'update : '.print_r($temp,true));
		}
		echo json_encode($response);
	}
}
?>