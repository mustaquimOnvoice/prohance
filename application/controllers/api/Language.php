<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class language extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');		
    }
	
	//scheme photos
	function fetch_all(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		
		if(isset($_POST['device_token']) && isset($_POST['mr_id'])){	
			$response ['message'] = "done";
			$response ['result'] =  "language List";
			
			$select = array('name','code');
			$where = array('status' => '1');
			$response['data'] = $this->Base_Models->GetAllValues ("languages",$where,$select);
			// log_message('error', 'update : '.print_r($temp,true));
		}
		echo json_encode($response);
	}
}
?>