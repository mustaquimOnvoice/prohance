<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class attendance extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		if(!empty($_POST['atsm_code']) && !empty($_POST['device_token'])){
			$this->api_model->check_token('atsm',$_POST['atsm_code'],$_POST['device_token']);
		}else{
			$response ['message'] = "fail";
			$response ['result'] =  "Param not found";
			echo json_encode($response);
			die();
		}
		
    }

    function index(){

        echo "call";
    }
	
	function check(){
        $response ['message'] = "fail";
		$response ['result'] =  "Param Required";
		if(!empty($_POST['atsm_code']) && !empty($_POST['attendance_date'])){
			$attendance_date = date('Y-m-d',strtotime($_POST['attendance_date']));
			$this->api_model->check_attendance('atsm_attendance',$_POST['atsm_code'],$attendance_date);
		}
		echo json_encode($response);
    }
	
	function add(){
		$response ['message'] = "fail";
		$response ['result']="";
		if( isset($_POST['device_token']) && 
			isset($_POST['atsm_id'])  && 
			isset($_POST['atsm_code'])  && 
			isset($_POST['weekoff'])  && 
			isset($_POST['inorout']) && 
			isset($_POST['attendance_date']) &&
			isset($_POST['rt_id']) &&
			isset($_POST['rt_code']) &&
			isset($_POST['rt_firmname']) &&
			isset($_POST['lati']) &&
			isset($_POST['longi'])){
				$TableValues['device_token']=$_POST['device_token'];
				$TableValues['atsm_id']=$_POST['atsm_id'];
				$TableValues['atsm_code']=$_POST['atsm_code'];
				$TableValues['rt_id']=$_POST['rt_id'];
				$TableValues['rt_code']=$_POST['rt_code'];
				$TableValues['rt_firmname']=$_POST['rt_firmname'];
				$TableValues['weekoff']=$_POST['weekoff'];
				$TableValues['inorout']=$_POST['inorout'];
				$TableValues['attendance_date']= date('Y-m-d',strtotime($_POST['attendance_date']));
				$TableValues['attendance_time']= date('h:i:s',strtotime($_POST['attendance_date']));
				$TableValues['lati']=$_POST['lati'];
				$TableValues['longi']=$_POST['longi'];
				$TableValues['created_at']= date('Y-m-d h:i:s');

				$id= $this->Base_Models->AddValues ( "atsm_attendance", $TableValues );
				$insert_id = $this->db->insert_id();
				//insert photo
				foreach ($_FILES as $key => $value) {
					$imgresponse = $this->uploadImageFile($value,$insert_id ,"1");            
				}
				$response ['message'] = "done";
				$response ['result']="Successfully Attendence";
				if($_POST['weekoff'] == '1'){
					$response ['data'] = array('weekoff' => '1', 'attendance_date' => date('Y-m-d h:i:s',strtotime($_POST['attendance_date'])));
				}else{
					$response ['data']=$this->input->post();					
					$response ['data']['image']=$imgresponse['image_url'];
				}
		}
		// log_message('error', 'img  file: '.print_r($_POST,true));
	   echo json_encode($response);
    }
	
	function uploadImageFile($file,$user_id,$type=1) {
        $response ['message'] = "fail";
        if (isset ( $user_id) ) {
            if (isset ( $file ) && $file ['error'] == 0) {
                if (! file_exists ( APPPATH . "../uploads/" . $user_id )) {
                    mkdir ( APPPATH . "../uploads/" . $user_id, 0777, true );
                }
                    log_message('error', 'img  file: '.print_r($file,true));
                $temp = "uploads/" . $user_id . "/images_unitglo_mobile-" . $this->generate_random_string ( 10 );
                if ($temp != "") {
                    $image_folder = APPPATH . "../" . $temp;
                    list ( $a, $b ) = explode ( '.', $file ['name'] );
                    $result = $this->imageCompress ( $file ['tmp_name'], $image_folder . "." . $b, 80 );
                    if ($result != '') {
                        $response ['message'] = "done";
                        $response ['image_url'] = base_url ( $temp . "." . $b );

                        $TableValues ['ref_code'] = $user_id;
                        $TableValues ['type'] = $type;
                        $TableValues ['image_url'] = $response ['image_url'];

                        $response ['upload_id'] = $this->Base_Models->AddValues ( "images", $TableValues );
                    }
                }
            }
        }
        //log_message('error', 'img : '.print_r($response,true));
        return  $response ;
    }
	
	function monthly(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		
		$year = date('Y',strtotime($_POST['year_month']));
		$month = date('m',strtotime($_POST['year_month']));
		$total_days = cal_days_in_month(CAL_GREGORIAN,$month,$year); //convert to days
		$response ['total_days'] =  $total_days;
				
		$select = array('atsm_id','atsm_code','fname','username','email','contact','pan_no');
		if(!empty($_POST['atsm_id'] && !empty($_POST['atsm_code']) && !empty($_POST['year_month']))){
			$response ['message'] = "done";
			$response ['result'] =  "Monthly attendence";
			
			$v=1;//half day
			$w=1;//present false
			$y=1;//present true
			$z=1;//weekoff
			$response['total_half_day'] = 0;
			$response['total_present'] = 0;
			$response['total_pending'] = 0;
			$response['total_weekoff'] = 0;
			$att = array();
			for ($x = 1; $x <= $total_days; $x++) {
			    $attendance[$x]['pm_time'] = '00:00:00';
				$attendance_date = "$year-$month-$x";
				$attendance[$x]= $this->api_model->check_tsm_attendance('atsm_attendance',$_POST['atsm_code'],$attendance_date);
				$attendance[$x]['date'] = strtotime($attendance_date.' '.$attendance[$x]['am_time'])*1000;
				$attendance[$x]['half_day'] = 'false';
				
				if($attendance[$x]['pending'] == 'false' && $attendance[$x]['weekoff'] == 'false'){
					$response['total_present'] = $w++;
					$last_time = date('H:i:s',strtotime($attendance[$x]['am_time']) + 60*60*8);
					if($attendance[$x]['pm_time'] == '' || $attendance[$x]['pm_time'] < $last_time){
						$response['total_half_day'] = $v++;
						$attendance[$x]['half_day'] = 'true';
					}
				}
				if($attendance[$x]['pending'] == 'true'){
					$response['total_pending'] = $y++;
				}
				if($attendance[$x]['weekoff'] == 'true'){
					$response['total_weekoff'] = $z++;
				}
				array_push($att,$attendance[$x]);
			}
				$response['attendance'] = $att;
		}
		echo json_encode($response);
	}
}
?>