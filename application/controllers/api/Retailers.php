<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class retailers extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		// if(!empty($_POST['mr_code']) && !empty($_POST['device_token'])){
			// $this->api_model->check_token('mr',$_POST['mr_code'],$_POST['device_token']);
		// }else{
			// $response ['message'] = "fail";
			// $response ['result'] =  "Param not found";
			// echo json_encode($response);
			// die();
		// }		
    }

    function index(){

        echo "call";
    }
	

	function rt_list(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		$rt_list=null;

		$select = array('rt_id','rt_code','tsm_id','firmname','fname','lname','username','email','contact','pan_no','gst_no','(select count(imei) from tbl_sales_to_rt where rt_id = retailer.rt_id AND item_status = "0" limit 1) as imei_count');
		if(isset($_POST['atsm_id'])){
			$rt_list= $this->Base_Models->GetAllValues ( "retailer" ,array('tsm_id' => $_POST['atsm_id']),$select);
			// foreach ($event_list as $key => $value) {
				// $event_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"1")  );
				// $event_list[$key]["images"]=json_encode($event_images);
			// }
			$response ['message'] = "done";
			$response ['result'] =  "Retailer List";
		}else{
			$rt_list= $this->Base_Models->GetAllValues ( "retailer",'',$select );
			// foreach ($event_list as $key => $value) {
					// $event_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"1")  );
					// $event_list[$key]["images"]=json_encode($event_images);
				// }    
			$response ['message'] = "done";
			$response ['result'] =  "Retailer List";
		}
		$response ['rt_list'] =  $rt_list;
		echo json_encode($response);
	}
	
	function rt_stock(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		$stock_list=null;

		$select = array('item_id','item_code','imei','rt_id','rt_code');
		$where = array('rt_code' => $_POST['rt_code'],'item_status' => '0');
		if(isset($_POST['rt_code'])){
			$stock_list= $this->Base_Models->GetAllValues ( "tbl_sales_to_rt" ,$where,$select);
			$response ['message'] = "done";
			$response ['result'] =  "Stock List";
		}
		$response ['stock_list'] =  $stock_list;
		echo json_encode($response);
	}
	
	function search_imei(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		$stock_list=null;

		$select = array('item_id','item_code','imei','nd_id','nd_code','d_id','d_code','rt_id','rt_code','c_id','c_code','level_type');
		$where = array('imei' => $_POST['imei']);
		if(isset($_POST['imei'])){
			$stock_list= $this->Base_Models->GetAllValues ( "tbl_item_sales" ,$where,$select);
			$response ['message'] = "done";
			$response ['result'] =  "IMEI Details";
			switch($stock_list[0]['level_type']){
				case 0:
					$response ['status'] = 'Admin';
					break;
				case 1:
					$response ['status'] = 'ND';
					break;
				case 2:
					$response ['status'] = 'D';
					break;
				case 3:
					$response ['status'] = 'RT';
					break;
				case 4:
					$response ['status'] = 'Activated';
					break;
			}
			$response ['result'] =  "IMEI Details";			
		}
		$response ['stock_list'] =  $stock_list;
		echo json_encode($response);
	}
	
	public function activate_imei()
	{
		if(empty($_POST['date']) || empty($_POST['cilent_name']) || empty($_POST['cilent_mob']) || empty($_POST['imei']) || empty($_POST['atsm_id'])){
			$response ['message'] = "fail";
			$response ['result'] =  "Param not found";
			echo json_encode($response);
			die();
		}
		
		//check already activated, exist, or not in retailer stock
		$this->api_model->check_activate_imei('tbl_item_sales',$_POST['imei'],$_POST['device_token']);
		
		$current_date = date("Y-m-d",strtotime($this->input->post('date')));
		$cilent_name = $this->input->post('cilent_name');
		$cilent_mob = $this->input->post('cilent_mob');
		$imei_code = trim($this->input->post('imei'));
		
		$this->db->trans_begin();// transaction start
		
		//insert client
		$cldata  = array('tsm_id' => $this->input->post('atsm_id'),
						'fname'   => $cilent_name,
						'contact'   => $cilent_mob,
						'inserted_on'=> date('Y-m-d h:i:s')
						);
		$this->db->insert("client",$cldata);
		$insert_id = $this->db->insert_id();
		//fetch client details
		$cl = $this->db->query("select c_id,c_code from client where c_id='$insert_id'");
		$client_array = $cl->result_array();
		$c_id = $client_array[0]['c_id'];
		$c_code = $client_array[0]['c_code'];
		
		//insert tbl_sales_to_c
		$rs         = $this->db->query("select item_id,item_code from tbl_items where imei='$imei_code'");
		$item_array = $rs->result_array();
		$item_id = $item_array[0]['item_id'];
		$item_code  = $item_array[0]['item_code'];
		$date =	$current_date;

		$data  = array('item_id'  	=> $item_id,
					'item_code	'   => $item_code,
					'imei'   		=> $imei_code,
					'c_id'   		=> $c_id,
					'c_code'    	=> $c_code,
					'upload_date'   => $date,
					'inserted_on'   => date('Y-m-d h:i:s')
					);
		$this->db->insert("tbl_sales_to_c",$data);
		$id= $this->db->insert_id();
		//insert photo
		foreach ($_FILES as $key => $value) {
			$imgresponse = $this->uploadImageFile($value,$id ,"2");            
		}
		
		//update tbl_item_sales
		$update_array	=	array(
			'level_type'  	=> '4',
			'c_id'      => $c_id,
			'c_code'	  => $c_code,
			'c_date'   => $date,
			'updated_on'	=> date("Y-m-d H:i:s")
			);
		$where_array	=	array('imei'=> $imei_code);
		$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
		
		//update tbl_sales_to_rt
		$update_array = array(
						'item_status'=>'1',
						'updated_on'=>$current_date
						);
		$where_array = array('imei'=> $imei_code);
		$this->base_models->update_records('tbl_sales_to_rt',$update_array,$where_array);
				
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback(); //rollback
			$response['status'] = 'fail';
			$response['message'] = 'Someting went worng try again';
		}else{
			$this->db->trans_commit(); //commit
			$response['status'] = 'done';
			$response['message'] = 'Activated successfully';
		}
		echo json_encode($response);
	}
	
	function uploadImageFile($file,$user_id,$type=2) {
        $response ['message'] = "fail";
        if (isset ( $user_id) ) {
            if (isset ( $file ) && $file ['error'] == 0) {
                if (! file_exists ( APPPATH . "../uploads/" . $user_id )) {
                    mkdir ( APPPATH . "../uploads/" . $user_id, 0777, true );
                }
                    log_message('error', 'img  file: '.print_r($file,true));
                $temp = "uploads/" . $user_id . "/images_unitglo_mobile-" . $this->generate_random_string ( 10 );
                if ($temp != "") {
                    $image_folder = APPPATH . "../" . $temp;
                    list ( $a, $b ) = explode ( '.', $file ['name'] );
                    $result = $this->imageCompress ( $file ['tmp_name'], $image_folder . "." . $b, 80 );
                    if ($result != '') {
                        $response ['message'] = "done";
                        $response ['image_url'] = base_url ( $temp . "." . $b );

                        $TableValues ['ref_code'] = $user_id;
                        $TableValues ['type'] = $type;
                        $TableValues ['image_url'] = $response ['image_url'];

                        $response ['upload_id'] = $this->Base_Models->AddValues ( "images", $TableValues );
                    }
                }
            }
        }
        //log_message('error', 'img : '.print_r($response,true));
        return  $response ;
    }
	
	//retailer visit
	public function visit(){
		$response ['message'] = "fail";
		$response ['result']="";
		if( isset($_POST['device_token']) && 
			isset($_POST['atsm_id'])  && 
			isset($_POST['atsm_code'])  && 
			isset($_POST['rt_id'])  && 
			isset($_POST['rt_code'])  && 
			isset($_POST['visit_date_time']) && 
			isset($_POST['lati']) && 
			isset($_POST['longi'])){
				$TableValues['device_token']=$_POST['device_token'];
				$TableValues['atsm_id']=$_POST['atsm_id'];
				$TableValues['atsm_code']=$_POST['atsm_code'];
				$TableValues['rt_id']=$_POST['rt_id'];
				$TableValues['rt_code']=$_POST['rt_code'];
				$TableValues['lati']=$_POST['lati'];
				$TableValues['longi']=$_POST['longi'];
				$TableValues['visit_date']= date('Y-m-d',strtotime($_POST['visit_date_time']));
				$TableValues['visit_time']= date('h:i:s',strtotime($_POST['visit_date_time']));
				$TableValues['created_at']= date('Y-m-d h:i:s');

				$id= $this->Base_Models->AddValues ( "rt_visit", $TableValues );
				$insert_id = $this->db->insert_id();
				//insert photo
				foreach ($_FILES as $key => $value) {
					$imgresponse = $this->uploadImageFile($value,$insert_id ,"3");            
				}
				$response ['message'] = "done";
				$response ['result']="Successfully Visited";
				$response ['data']=$this->input->post();					
				$response ['data']['image']=$imgresponse['image_url'];
		}
		// log_message('error', 'img  file: '.print_r($_POST,true));
	   echo json_encode($response);
	}
	
	//get visit list by date
	function visit_list(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		$visit_list=null;

		$select = array('rt_visit_id','rt_id','rt_code','(select firmname from retailer where rt_id = rt_visit.rt_id limit 1) as rt_firmname','visit_date','visit_time','lati','longi','(select image_url from images where type = "3" AND ref_code = rt_visit.rt_visit_id limit 1) as image');
		if(isset($_POST['atsm_id'])){
			$visit_list= $this->Base_Models->GetAllValues ( "rt_visit" ,array('atsm_id' => $_POST['atsm_id'],'visit_date' => date('Y-m-d',strtotime($_POST['visit_date']))),$select);
			// foreach ($visit_list as $key => $value) {
				// $images= $this->Base_Models->GetAllValues ( "images" ,array("ref_code"=>$value['rt_visit_id'] ,"type"=>"3")  );
				// $visit_list[$key]["images"]=json_encode($images);
				// $visit_list[$key]["images"]=$images;
			// }
			$response ['message'] = "done";
			$response ['result'] =  "Visit List";
		}
		$response ['visit_list'] =  $visit_list;
		echo json_encode($response);
	}
	
	//get visit list by Retailer id
	function visit_report_to_rt(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		$visit_list=null;

		$select = array('rt_visit_id','visit_date','visit_time','lati','longi','(select image_url from images where type = "3" AND ref_code = rt_visit.rt_visit_id limit 1) as image');
		if(isset($_POST['rt_id'])){
			$visit_list= $this->Base_Models->GetAllValues ( "rt_visit" ,array('rt_id' => $_POST['rt_id']),$select);
			$response ['message'] = "done";
			$response ['result'] =  "Visit List";
		}
		//get retailer details
		$select_rt = array('rt_id','rt_code','firmname');
		$rt_details = $this->Base_Models->GetAllValues ( "retailer" ,array('rt_id' => $_POST['rt_id']),$select_rt);
		$response ['rt_id'] =  $rt_details[0]['rt_id'];
		$response ['rt_code'] =  $rt_details[0]['rt_code'];
		$response ['rt_firmname'] =  $rt_details[0]['firmname'];
		$response ['data'] =  $visit_list;
		echo json_encode($response);
	}
	
	//count imei activation of retailer under TSM
	function imei_activation_count(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param required";
		
		if(!empty($_POST['atsm_id'] && !empty($_POST['atsm_code']))){			
			$activation_count= $this->api_model->imei_activation_count(trim($_POST['atsm_id']));
			$response ['message'] = "done";
			$response ['result'] =  "Activated list";
			$response ['data'] =  $activation_count;
		}
		echo json_encode($response);
	}
}
?>