<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class login_old extends Base_Controller {
	public function __construct() {
		parent::__construct ();
	}

	/**
	 * Registration of New User
	 *
	 * @param
	 *        	Parameters as
     *          user_mobile_no
     *          device_token
     * 
	 * @return message string and result
	 */

    public function status() {
        $response ['message'] = "done";
        // $response ['version'] =  "2.0";
        $response ['maintenance_mode'] =  false;
        //$response ['active_meetings'] =  $this->Base_Models->GetSingleDetails('tbl_status', array('name' => 'Meetings'), "status")->status;
        echo json_encode($response);
    }
	
	//Login ASM/TSM
	public function index() {
		$response ['message'] = "fail";
		$response ['result'] =  "Param is reqired";
		if( isset($_POST['atsm_code']) && isset($_POST['device_token']) && isset($_POST['password']) ){
			$atsm_code = $_POST['atsm_code'];
			$password = md5(trim($_POST['password']));
			
			$this->load->model("adminmaster_models");
			$select = array('atsm_id','atsm_code','nd_id as ndid','level_type','upline_id as upid','firmname','fname','mname','lname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select atsm_code from atsm where upid = atsm_id Order by atsm_id desc limit 1) as upline_code','(select nd_code from ndistributor where ndid = nd_id Order by nd_id desc limit 1) as nd_code','(select firmname from ndistributor where ndid = nd_id Order by nd_id desc limit 1) as nd_firmname','(select date_time from recent_login_user where user_code = atsm_code AND type = "1" Order by id desc limit 1) as last_login','(select state_name from area where state = state_id Order by area_id desc limit 1) as state_name','(select city_name from area where city = city_id Order by area_id desc limit 1) as city_name','(select taluka_name from area where taluka = taluka_id Order by area_id desc limit 1) as taluka_name');
			$where = array('status !=' => '2','atsm_code' => $atsm_code,'password' => $password);		
			$temp1 = $this->adminmaster_models->atsm_list($select,'atsm', $where,'atsm_id','1');
				
			if(count($temp1)==0){
				$response ['message'] = "fail";
				$response ['result'] =  "Check login details";
			}else{
				$TableValues["device_token"] = $_POST['device_token'];
				$this->Base_Models->UpadateValue ( "atsm", array ("device_token"=>$_POST['device_token']), array("atsm_code" => $atsm_code) );
				$response ['message'] = "done";
				$response ['result'] = $temp1[0];
			}
		}
		echo json_encode ($response);
	}
	
	public function registration() {
		$response ['message'] = "fail";
		$response ['result'] =  "OTP not send";
        // echo json_encode($_POST);
        // exit();
        if( isset($_POST['user_mobile_no']) && isset($_POST['device_token']) ){
           $TableValues['user_mobile_no']= $_POST['user_mobile_no'];
			$type = $_POST['type'];
				if($type == 'user'){
					$temp1 = $this->Base_Models->GetAllValues("mpyc_users_temp",$TableValues);
					if(count($temp1)==0){
						$response ['message'] = "fail";
						$response ['result'] =  "User not found";
					}else{
						
						$temp = $this->Base_Models->GetAllValues("mpyc_users",$TableValues);
						$TableValues['device_token']= $_POST['device_token'];
						$otp = "";
						$send_message = "Hi User, your OTP for MPYC is ";

						if(count($temp)==0){
							$str = $this->generate_random_string ( 4, 'num' );
							if($TableValues['user_mobile_no'] == '9503346902'){
								$str="1234";
							}
							$TableValues['user_otp']=$str;
							$TableValues['user_status']="1";
							$TableValues['user_is']="new";
							$otp=$str;
							
				
							$temp = $this->Base_Models->AddValues ( "mpyc_users", $TableValues);
							$response ['type'] = "new";
							$response ['message'] = "done";
							$response ['result'] =  "OTP send";
							

						}else {
							
							if($temp[0]['user_otp']==""){  
								$TableValues['device_token']= $_POST['device_token'];
								$str = $this->generate_random_string ( 4, 'num' );
								if($TableValues['user_mobile_no'] == '9503346902'){
									$str="1234";
								}
								$otp=$str;
								
								 $this->Base_Models->UpadateValue ( "mpyc_users", array (
									"user_otp" =>$str,
									"device_token"=>$TableValues["device_token"],
									"user_status"=>1
								), array (
									"user_id" => $temp[0]['user_id']
								) );
							}else{
								$this->Base_Models->UpadateValue ( "mpyc_users", array (
										"device_token"=>$TableValues["device_token"],
										"user_status"=>1

								), array (
										"user_id" => $temp[0]['user_id']
								) );
								$otp=$temp[0]['user_otp'];
							}
							$response ['message'] = "done";
							$response ['result'] =  "OTP re-send ";
			   
						}
							$send_message=$send_message.$otp." ";
							$this->message_send ( $send_message,  $TableValues['user_mobile_no']);
					}
				}else{
					$temp = $this->Base_Models->GetAllValues("mpyc_district_admin",$TableValues);
					$TableValues['device_token']= $_POST['device_token'];
					$otp = "";
					$send_message = "Hi District Admin, your OTP for MPYC is ";

					if(count($temp)==0){
						$response ['message'] = "fail";
						$response ['result'] =  "District Admin not found";        

					}else {
						
						if($temp[0]['user_otp']==""){  
							$TableValues['device_token']= $_POST['device_token'];
							$str = $this->generate_random_string ( 4, 'num' );
							$otp=$str;
							//$str="1234";
							
							 $this->Base_Models->UpadateValue ( "mpyc_district_admin", array (
								"user_otp" =>$str,
								"device_token"=>$TableValues["device_token"],
								"user_status"=>1
							), array (
								"user_id" => $temp[0]['user_id']
							) );
						}else{
							$this->Base_Models->UpadateValue ( "mpyc_district_admin", array (
									"device_token"=>$TableValues["device_token"],
									"user_status"=>1

							), array (
									"user_id" => $temp[0]['user_id']
							) );
							$otp=$temp[0]['user_otp'];
						}
						$response ['message'] = "done";
						$response ['result'] =  "OTP re-send ";
						
						$send_message=$send_message.$otp." ";
						$this->message_send ( $send_message,  $TableValues['user_mobile_no']);
		   
					}
				}
            
        }else {
            $response ['message'] = "fail";
        }

		echo json_encode ( $response );
    }


    public function otp_varification(){
		$response ['message'] = "fail";
		$response ['result'] =  "OTP not varify";
        if( isset($_POST['user_mobile_no'])  && isset($_POST['device_token']) && isset($_POST['user_otp'])){
           $TableValues['user_mobile_no']= $_POST['user_mobile_no'];
           $TableValues['user_otp']= $_POST['user_otp']; 
		
			$type = $_POST['type'];
				if($type == 'user'){
					$temp = $this->Base_Models->GetAllValues("mpyc_users",$TableValues,"device_token,age,fname,lname,user_id,user_is,email as emial,user_type as userType,user_address,user_mobile_no,designation,assembly,user_role,user_district,facebook,twitter ,(SELECT image_url FROM  `images` where ref_id=mpyc_users.user_id and type=0 ORDER BY id DESC LIMIT 1) as image_url");
					$TableValues['device_token']= $_POST['device_token'];
					$TableValues['user_otp']="";
					$TableValues['user_status']=2;
					
					if(count($temp)==1){
						$response ['type'] = $temp[0]['user_is'];
						$response ['message'] = "done";

						$this->Base_Models->UpadateValue ( "mpyc_users",$TableValues, array (
							"user_id" => $temp[0]['user_id']
						) );

							$response ['result'] = json_encode($temp[0]);


					}else{
						$response ['message'] = "fail";
					}
				}else{
					$temp = $this->Base_Models->GetAllValues("mpyc_district_admin",$TableValues,"device_token,age,fname,lname,user_id,user_is,email as emial,user_type as userType,user_address,user_mobile_no,designation,assembly,user_role,facebook,twitter,(SELECT image_url FROM  `images` where ref_id=mpyc_district_admin.user_id and type=3 ORDER BY id DESC LIMIT 1) as image_url, (SELECT GROUP_CONCAT(districtName) FROM mpyc_districts where districtAdminId=mpyc_district_admin.user_id ORDER BY id ) as user_district");
					$TableValues['device_token']= $_POST['device_token'];
					$TableValues['user_otp']="";
					$TableValues['user_status']=2;
					
					if(count($temp)==1){
						$response ['type'] = $temp[0]['user_is'];
						$response ['message'] = "done";

						$this->Base_Models->UpadateValue ( "mpyc_district_admin",$TableValues, array (
							"user_id" => $temp[0]['user_id']
						) );

							$response ['result'] = json_encode($temp[0]);


					}else{
						$response ['message'] = "fail";
					}
				}
		}else{
			$response ['message'] = "fail";
		}
        
		echo json_encode ( $response );
    }




    
}
?>