<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Mr extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 3){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}

	//---------- MR ------------//
	public function add_mr()
	{
		$this->renderView('Admin/Mr/add-mr');
	}
	
	public function insert_mr()
	{
		$this->form_validation->set_rules('fname', 'Name', 'trim|required');
		$this->form_validation->set_rules('mr_code', 'MR Code', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('hq', 'HQ', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('division', 'Division', 'trim|required');
		$this->form_validation->set_rules('designation', 'Designation', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'fname'=>$this->input->post('fname'),
						'mr_code'=>$this->input->post('mr_code'),
						'mobile_no'=> $this->input->post('contact'),
						'email'=>$this->input->post('email'),
						'hq'=>$this->input->post('hq'),
						'state'=>$this->input->post('state'),
						'division'=>$this->input->post('division'),
						'designation'=>$this->input->post('designation'),
						'created_by'=>$this->session->userdata('user_type'),
						'user_status'=>'2',
						'created_at'=>$current_date
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('mr',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(site_url('/Mr/mr_list'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
		$this->renderView('Admin/Mr/add-mr');
	}
	
	public function mr_list()
	{	
		// $select = array('mr_id','mr_code','fname','email','contact','user_status','created_at','(select image_url from images where ref_code = mr.mr_id AND type = "1" Order by id desc limit 1) as pro_pic','(select image_url from images where ref_code = mr.mr_id AND type = "3" Order by id desc limit 1) as frame_pic');
		$select = array('mr_id','mr_code','fname','email','hq','state','division','designation','mobile_no','user_status','created_at');
		$where = array('user_status !=' => '3');
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Mr/mr_list";
		$config["total_rows"] = $this->base_models->get_count('mr_id','mr', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagi_data($select,'mr', $where,'mr_id',$config["per_page"], $page);     
		//Pagination End
		$pagedata['fdate'] = '';
		$pagedata['todate'] = '';
		$pagedata['select'] = array('mr_id'=>'');
		$pagedata['delete_link'] = 'Mr/delete_mr';
		$pagedata['mr_data'] = $this->base_models->GetAllValues('mr', $where, array('mr_id','mr_code','fname','email','mobile_no'));
		$this->renderView('Admin/Mr/mr_list',$pagedata);
	}

	public function mr_list_sess()
	{
		$fdate = date('Y-M-d');
		$todate = date('Y-M-d');
		$ranges = '';
		$pagedata["links"] = '';	
		$pagedata['results'] = array();
		
		// if(!empty($this->session->userdata('fdate'))){
			
			if(!empty($_POST)){
				$ranges = explode('-',$this->input->post('daterange'));
				$fdate = date('Y-m-d', strtotime($ranges[0])).' 00:00:00';
				$todate = date('Y-m-d', strtotime($ranges[1])).' 23:59:00';
				$mr_id = $_POST['mr_id'];

				$this->session->set_userdata('fdate',$fdate);
				$this->session->set_userdata('todate',$todate);
				$this->session->set_userdata('mr_id',$mr_id);
				
				// zip file download
				// if(@$_POST['submit']=='creatzip')
				// {
					// $this->download_zip($fdate,$todate);		
					// redirect('Doctor/download_zip');
				// }
			}else{
				$fdate = $this->session->userdata('fdate');
				$todate = $this->session->userdata('todate');
				$mr_id = $this->session->userdata('mr_id');
			}

			
			// $select = array('mr_id','mr_code','fname','email','contact','user_status','created_at','(select image_url from images where ref_code = mr.mr_id AND type = "1" Order by id desc limit 1) as pro_pic','(select image_url from images where ref_code = mr.mr_id AND type = "3" Order by id desc limit 1) as frame_pic');
			$select = array('mr_id','mr_code','fname','email','hq','state','division','designation','mobile_no','user_status','created_at');
			$where = array('user_status !=' => '3','created_at  >='=> "$fdate",'created_at  <='=> "$todate");
			if(!empty($mr_id)){
				$where = array( 'mr_id'=>$mr_id );
			}
			
			if(@$_POST['submit']=='genexl')
			{
				$data['data'] = $this->base_models->GetAllValues('mr', $where, $select);
				$this->generate_mr_excel($data['data']);
			}

			//Pagination Start
			$config = array();
			$config["base_url"] = site_url() . "/Mr/mr_list_sess";
			$config["total_rows"] = $this->base_models->get_count('mr_id','mr', $where);
			$config["per_page"] = 10;
			$config["uri_segment"] = 3;
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$pagedata["links"] = $this->pagination->create_links();
			$pagedata['results'] = $this->base_models->get_pagi_data($select,'mr', $where,'mr_id',$config["per_page"], $page);
		// }
		$pagedata['filter'] = true;
		$pagedata['fdate'] = date('Y-m-d', strtotime($fdate));
		$pagedata['todate'] = date('Y-m-d', strtotime($todate));
		$pagedata['delete_link'] = 'Mr/delete_mr';
		$pagedata['mr_data'] = $this->base_models->GetAllValues('mr', $where, array('mr_id','mr_code','fname','email','mobile_no'));
		$pagedata['select'] = array('mr_id'=>$mr_id);
		$this->renderView('Admin/Mr/mr_list',$pagedata);
	}

	public function generate_mr_excel($param1){
		foreach (glob(APPPATH.'../uploads/admin/excel/*.xlsx') as $del) { // remove previous zip file
			unlink($del);
		}
		// create file name
		$fileName = 'MrList'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'MR code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Contact');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'HQ');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'State');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Division');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Designation');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Date');
		
		// set Row
		$rowCount = 2;

		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['mr_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['fname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['mobile_no']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['email']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['hq']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['state']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['division']);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['designation']);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, date('d-M-y', strtotime($element['created_at'])));			
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}

	public function edit_mr()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->GetSingleDetails('mr', array('mr_id' => $id), $select = "*");
				
		$this->renderView('Admin/Mr/edit-mr',$pagedata);
	}
	
	public function update_mr()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(site_url('/Mr/mr_list')); 
		}
		
		$this->form_validation->set_rules('fname', 'Name', 'trim|required');
		$this->form_validation->set_rules('mr_code', 'MR Code', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('hq', 'HQ', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('division', 'Division', 'trim|required');
		$this->form_validation->set_rules('designation', 'Designation', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			
			if($this->form_validation->run())
			{				
				$update_array=array(
						'fname'=>$this->input->post('fname'),
						'mr_code'=>$this->input->post('mr_code'),
						'mobile_no'=> $this->input->post('contact'),
						'email'=>$this->input->post('email'),
						'hq'=>$this->input->post('hq'),
						'state'=>$this->input->post('state'),
						'division'=>$this->input->post('division'),
						'designation'=>$this->input->post('designation'),
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('mr_id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('mr',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(site_url('/Mr/edit_mr/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_mr()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'user_status'=>'3',
							'deleted_on'=>$current_date
							);
		$where_array = array('mr_id'=>$id);
		if($this->base_models->update_records('mr',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
}
