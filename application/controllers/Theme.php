<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Theme extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}

	//---------- MR ------------//
	public function add_theme()
	{
		$this->renderView('Admin/Theme/add-theme');
	}
	
	public function insert_theme()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'theme_name'=>$this->input->post('name'),
						'status'=>'1',
						'created_by'=>$this->session->userdata('user_type'),
						'created_at'=>$current_date
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('themes',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(site_url('/theme/theme_list'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
		$this->renderView('Admin/Themes/add-themes');
	}
	
	public function theme_list()
	{	
		$select = array('theme_id','theme_name','status','created_at');
		$where = array('status' => '1');
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Theme/theme_list";
		$config["total_rows"] = $this->base_models->get_count('theme_id','themes', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagi_data($select,'themes', $where,'theme_id',$config["per_page"], $page);     
		//Pagination End
		$pagedata['delete_link'] = 'Theme/delete_theme';
		$this->renderView('Admin/Theme/theme_list',$pagedata);
	}
	
	public function edit_theme()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->GetSingleDetails('themes', array('theme_id' => $id), $select = "*");				
		$this->renderView('Admin/Theme/edit-theme',$pagedata);
	}
	
	public function update_theme()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(site_url('/Theme/theme_list')); 
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
	
		$error='';
			
			if($this->form_validation->run())
			{				
				$update_array=array(
						'theme_name'=>$this->input->post('name'),
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('theme_id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('themes',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(site_url('/Theme/edit_theme/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_theme()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'0',
							'deleted_on'=>$current_date
							);
		$where_array = array('theme_id'=>$id);
		if($this->base_models->update_records('themes',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
}
