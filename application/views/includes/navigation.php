<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Preloader --
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<!-- Preloader	
<div class="preloader">
	<svg class="circular" viewBox="25 25 50 50">
		<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
	</svg>
</div> -->
<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse slimscrollsidebar">
		<ul class="nav" id="side-menu">
			<?php if($this->session->userdata('login_type') == 'Super_Admin'){?>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">MR<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Mr/add_mr/');?>">Add MR</a> </li>
						<li> <a href="<?php echo site_url('Mr/mr_list/');?>">MR List</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Doctor's<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('doctor/dr_list/');?>">List</a></li>
						<li> <a href="<?php echo site_url('doctor/dr_list_pending/');?>">Pending</a></li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Themes<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Theme/add_theme/');?>">Add</a> </li>
						<li> <a href="<?php echo site_url('Theme/theme_list/');?>">List</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Template<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Template/add/');?>">Add</a> </li>
						<li> <a href="<?php echo site_url('Template/template_list/');?>">List</a> </li>
					</ul>
				</li>
			<?php }elseif($this->session->userdata('login_type') == 'Admin'){?>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">MR<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Mr/add_mr/');?>">Add MR</a> </li>
						<li> <a href="<?php echo site_url('Mr/mr_list/');?>">MR List</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Doctor's<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('doctor/dr_list/');?>">List</a></li>
						<li> <a href="<?php echo site_url('doctor/dr_list_pending/');?>">Pending</a></li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Template<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Template/add/');?>">Add</a> </li>
						<li> <a href="<?php echo site_url('Template/template_list/');?>">List</a> </li>
					</ul>
				</li>
			<?php }elseif($this->session->userdata('login_type') == 'Client'){?>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">MR<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Mr/add_mr/');?>">Add MR</a> </li>
						<li> <a href="<?php echo site_url('Mr/mr_list/');?>">MR List</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Doctor's<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('doctor/dr_list/');?>">List</a></li>
						<li> <a href="<?php echo site_url('doctor/dr_list_pending/');?>">Pending</a></li>
					</ul>
				</li>
			<?php }?>
		</ul>
	</div>
</div>
<!-- Left navbar-header end -->