<title>Edit Advertisement</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- page CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
<?php //die('s');?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Edit Advertisement</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- .row -->
		<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <form method="post" name="edit-form" id="edit-form" action="<?php echo site_url(); ?>/Adminmaster/update_nd/?id=<?php echo base64_encode($data->nd_id); ?>?>" enctype="multipart/form-data" data-toggle="validator">
			  <div class="row">
				  <div class="form-group col-sm-3">
					<label for="firmname" class="control-label">Firm Name</label>
					<input type="text" class="form-control" id="firmname" name="firmname" placeholder="Firm Name" data-error="Firm Name is required" value="<?php echo $data->firmname; ?>" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="fname" class="control-label">First Name</label>
					<input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" data-error="First Name is required" value="<?php echo $data->fname; ?>" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="mname" class="control-label">Middle Name</label>
					<input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name" value="<?php echo $data->mname; ?>">
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="lname" class="control-label">Last Name</label>
					<input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" value="<?php echo $data->lname; ?>" data-error="Last Name is required" required>
					<div class="help-block with-errors"></div>
				  </div>
			  </div>
			  <div class="row">
				  <div class="form-group col-sm-3">
					<label for="email" class="control-label">Primary Email</label>
					<input type="email" class="form-control" id="email" name="email" value="<?php echo $data->email; ?>" placeholder="Email" data-error="Bruh, that email address is invalid" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="email2" class="control-label">Secondary Email</label>
					<input type="email" class="form-control" id="email2" name="email2" value="<?php echo $data->email2; ?>" placeholder="Secondary Email">
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="contact" class="control-label">Primary Mobile</label>
					<input type="number" onchange="validate_mobile();" class="form-control" id="contact" name="contact" placeholder="Primary Mobile" data-error="Mobile Number required" required value="<?php echo $data->contact; ?>">
					<div class="help-block with-errors text-danger" id="contactError"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="contact2" class="control-label">Secondary Mobile</label>
					<input type="number" class="form-control" onchange="validate_mobile2();" id="contact2" name="contact2" placeholder="Secondary Mobile" value="<?php echo $data->contact2; ?>">
					<div class="help-block with-errors text-danger" id="contactError2"></div>
				  </div>
              </div>
			  <div class="row">
				  <div class="form-group col-sm-2">
					<label for="state" class="control-label">State</label>
					<select class="form-control" id="state" name="state" data-error="State required" required>
						<option value="">Select</option>
						<?php foreach($state_list as $state){?>
							<option <?php echo ($data->state == $state['state_id']) ? 'selected' : ''; ?> value="<?php echo $state['state_id'];?>"><?php echo $state['state_name'];?></option>
						<?php } ?>
					</select>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-2">
					<label for="city" class="control-label">City</label>
					<select class="form-control select2" id="city" name="city" data-error="City required" required>
						<option value="">Select</option>
						<?php foreach($city_list as $city){?>
							<option <?php echo ($data->city == $city['city_id']) ? 'selected' : ''; ?> value="<?php echo $city['city_id'];?>"><?php echo $city['city_name'];?></option>
						<?php } ?>
					</select>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="address" class="control-label">Address</label>
					<input type="text" class="form-control" id="address" name="address" value="<?php echo $data->address; ?>" placeholder="Address" data-error="Address required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-2">
					<label for="pan" class="control-label">PAN</label>
					<input type="text" class="form-control" id="pan" style="text-transform: uppercase;" onchange="ValidatePAN();" name="pan" placeholder="PAN" data-error="PAN required" required value="<?php echo $data->pan_no; ?>">
					<div class="help-block with-errors text-danger" id="panError"></div>
				  </div>
				  <div class="form-group col-sm-2">
					<label for="gst" class="control-label">GST</label>
					<input type="text" class="form-control" id="gst" name="gst" style="text-transform: uppercase;" onchange="ValidateGST();" placeholder="GST" data-error="GST required" required value="<?php echo $data->gst_no; ?>">
					<div class="help-block with-errors text-danger" id="gstError"></div>
				  </div>
              </div>
			  <div class="row">
				  <div class="form-group col-sm-3">
					<label for="acnt_name" class="control-label">Accountant Name</label>
					<input type="text" class="form-control" id="acnt_name" name="acnt_name" value="<?php echo $data->acnt_name; ?>" placeholder="Accountant Name" >
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="acnt_email" class="control-label">Accountant Email</label>
					<input type="email" class="form-control" id="acnt_email" name="acnt_email" value="<?php echo $data->acnt_email; ?>" placeholder="Accountant Email">
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="acnt_contact" class="control-label">Accountant Mobile</label>
					<input type="number" class="form-control" id="acnt_contact" name="acnt_contact" value="<?php echo $data->acnt_contact; ?>" placeholder="Accountant Mobile">
					<div class="help-block with-errors"></div>
				  </div>
              </div>
			  <div class="row">
				  <div class="form-group col-sm-6">
					<label for="inputPassword" class="control-label">New Password</label>
					<div class="row">
					  <div class="form-group col-sm-6">
						<input type="hidden" class="form-control" id="oldpassword" name="oldpassword" placeholder="Password" value="<?php echo $data->password; ?>">
						<input type="password" data-toggle="validator" class="form-control" data-minlength="6" name="inputPassword" id="inputPassword" placeholder="Password">
						<span class="help-block">Minimum of 6 characters</span> </div>
					  <div class="form-group col-sm-6">
						<input type="password" class="form-control"  name="inputPasswordConfirm" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Confirm Password">
						<div class="help-block with-errors"></div>
					  </div>
					</div>
				  </div>			  
				  <div class="form-group col-sm-3">
					<label for="status" class="control-label">Login Status</label>
					<select class="form-control" id="status" name="status" >
					  <option <?php if ($data->status== 0) echo 'selected' ; ?> value="0">Inactive</option>
					  <option <?php if ($data->status== 1) echo 'selected' ; ?> value="1">Active</option>
					</select>
				  </div>
              </div>			  
			  <!--<div class="form-group col-sm-3">
				<label for="image" class="control-label">Profile Pic</label>
				<input type="hidden" name="image1" id="image1" value="<?php //echo $data->profile_pic; ?>"/>
				<input type="file" class="form-control" id="image" name="image" value="<?php //echo set_value('profile_pic');?>">
			  </div>-->
              <div class="form-group">
				<div class="row">
                  <div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.row -->      
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/switchery/dist/switchery.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bower_components/multiselect/js/jquery.multi-select.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script>
 jQuery(document).ready(function() {
    // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());

        });
    // For select 2

    $(".select2").select2();
    $('.selectpicker').selectpicker();
     
     //Bootstrap-TouchSpin
              $(".vertical-spin").TouchSpin({
                verticalbuttons: true,
                verticalupclass: 'ti-plus',
                verticaldownclass: 'ti-minus'
            });
            var vspinTrue = $(".vertical-spin").TouchSpin({
                verticalbuttons: true
            });
            if (vspinTrue) {
                $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
            }
    
            $("input[name='tch1']").TouchSpin({
                min: 0,
                max: 100,
                step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: '%'
            });
            $("input[name='tch2']").TouchSpin({
                min: -1000000000,
                max: 1000000000,
                stepinterval: 50,
                maxboostedstep: 10000000,
                prefix: '$'
            });
            $("input[name='tch3']").TouchSpin();
           
            $("input[name='tch3_22']").TouchSpin({
                initval: 40
            });
    
            $("input[name='tch5']").TouchSpin({
                prefix: "pre",
                postfix: "post"
            });
           
      // For multiselect

      $('#pre-selected-options').multiSelect();      
      $('#optgroup').multiSelect({ selectableOptgroup: true });

      $('#public-methods').multiSelect();
      $('#select-all').click(function(){
        $('#public-methods').multiSelect('select_all');
        return false;
      });
      $('#deselect-all').click(function(){
        $('#public-methods').multiSelect('deselect_all');
        return false;
      });
      $('#refresh').on('click', function(){
      $('#public-methods').multiSelect('refresh');
        return false;
      });
      $('#add-option').on('click', function(){
        $('#public-methods').multiSelect('addOption', { value: 42, text: 'test 42', index: 0 });
        return false;
      });
              
 });
 
function validate_mobile(){
	var mob = $('#contact').val();
	// console.log(mob);
	$('#contactError').html('');
	if(mob.length==10){
		$('#contactError').html('');
		return true;
	}else{
		$('#contactError').html('Mobile number not valid..!');
		return false;
	}
}

function validate_mobile2(){
	var mob = $('#contact2').val();
	// console.log(mob);
	$('#contactError2').html('');
	if(mob.length==10){
		$('#contactError2').html('');
		return true;
	}else{
		$('#contactError2').html('Mobile number not valid..!');
		return false;
	}
}

//PAN validation
function ValidatePAN() {
	var pan = $('#pan').val();	
	var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/; 
	$('#panError').html('');
	if(regpan.test(pan)){ 
		$('#panError').html('');
		return true;
	} else { 
		$('#panError').html('Invalid Pan No.');
		return false;
	}
}

//GST validation
function ValidateGST() {
	var gstinVal = $('#gst').val();	
	// var gstinformat = new RegExp('^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$');
	$('#gstError').html('');
	// if (!gstinformat.test(gstinVal)) {
		// $('#gstError').html('Invalid GST No.');
		// return false;
    // }else{
		// $('#gstError').html('');
		// return true;
	// }
	if(gstinVal.length==15){
	    $('#gstError').html('');
		return true;
	}else{
	    $('#gstError').html('Invalid GST No.');
		return false;
	}
}
 </script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
