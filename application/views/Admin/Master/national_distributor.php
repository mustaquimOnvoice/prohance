<title>National Distributors</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
<script>
	var tblexport = true;
	console.log('tblexport: '+tblexport);
	console.log('CSS: assets/css/common/listing.css');
</script>
<!-- Listing CSS -->
<link href="<?php echo base_url();?>assets/css/common/listing.css"   rel="stylesheet">
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">National Distributors</h4>
        </div>
        <!--<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Table</a></li>
            <li class="active">Data Table</li>
          </ol>
        </div>-->
        <!-- /.col-lg-12 -->
      </div>
      <!-- /row -->
		<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>
      <div class="row">
		<div class="col-sm-12">
          <div class="white-box">
            <!--<h3 class="box-title m-b-0">Users</h3>-->
            <div class="table-responsive">
                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
							<th>ND Code</th>
							<th>Firm Name</th>
							<th>Name</th>
							<th>User Name</th>
							<th>Primary Email</th>
							<th>Secondary Email</th>
							<th>Primary Contact</th>
							<th>Secondary Contact</th>
							<th>Accountant Name</th>
							<th>Accountant Email</th>
							<th>Accountant Contact</th>
							<th>PAN</th>
							<th>GST</th>
							<th>State</th>
							<th>City</th>
							<th>Address</th>
							<th>Status</th>
							<!--<th>Photo</th>-->
							<th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1;
							if(count($results)>0){
								foreach($results as $result){ ?>
									<tr>
										<td><?php echo $i;?></td>
										<td><?php echo $result['nd_code']; ?></td>
										<td><?php echo $result['firmname']; ?></td>
										<td><?php echo $result['fname'].' '.$result['mname'].' '.$result['lname']; ?></td>
										<td><?php echo $result['username']; ?></td>
										<td><?php echo $result['email']; ?></td>
										<td><?php echo $result['email2']; ?></td>
										<td><?php echo $result['contact']; ?></td>
										<td><?php echo $result['contact2']; ?></td>
										<td><?php echo $result['acnt_name']; ?></td>
										<td><?php echo $result['acnt_email']; ?></td>
										<td><?php echo $result['acnt_contact']; ?></td>
										<td><?php echo $result['pan_no']; ?></td>
										<td><?php echo $result['gst_no']; ?></td>
										<td><?php echo $result['state_name']; ?></td>
										<td><?php echo $result['city_name']; ?></td>
										<td><?php echo $result['address']; ?></td>
										<td><?php echo ($result['status']== 0) ? 'Inactive' : 'Active'; ?></td>
										<!--<td> 
											<select class="form-control" id="status<?php //echo $i;?>" onchange="changeStatus('<?php //echo $result['nd_id']; ?>',<?php //echo $i;?>)">
											  <option <?php //if ($result['status']== 0) echo 'selected' ; ?> value="0">Inactive</option>
											  <option <?php //if ($result['status']== 1) echo 'selected' ; ?> value="1">Active</option>
											</select>															
										</td>
										<td>
											<a style="display:inline-block;color:#000;" title="img" <?php// echo empty($result['profile_pic']) ? ' ' : 'target="_blank"'; ?> href="<?php //echo empty($result['profile_pic']) ? '#' : base_url().'uploads/nd/profile/'.$result['profile_pic'] ?>" ><?php// echo empty($result['profile_pic']) ? 'No Photo' : 'click'; ?></a>
										</td>-->										
										<td>
											<a style="display:inline-block;color:#000;" title="Edit" href="<?php echo site_url(); ?>/Adminmaster/edit_nd?id=<?php echo base64_encode($result['nd_id']); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<a href="#" class="display:inline-block;color:#000;" title="Delete" data-url="<?php echo $delete_link;?>" id="delete<?php echo $i;?>" onclick="deleteRow('<?php echo $result['nd_id']; ?>',<?php echo $i;?>)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
										</td>
									</tr>
						<?php $i++; } } ?>                    
                    </tbody>
					<tfoot>
                     <tr>
                         <td colspan="19" class="text-left">
                           <span class="pagination"><?php echo $links; ?></span>
                         </td>                        
                     </tr>
                   </tfoot> 
                </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<!-- Load Adminmaster controller Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/admin/adminmaster.js"></script>
<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/common/listing.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
