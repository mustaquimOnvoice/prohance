<title>Import Products</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
<?php //die('s');?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Import Products </h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
		<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>
      <!-- .row -->
      <div class="row">
        <div class="col-sm-4">
          <div class="white-box">
            <form method="post" name="add-form" id="add-form" action="<?php echo site_url(); ?>/Adminitems/upload_items" enctype="multipart/form-data" data-toggle="validator" >
			  <div class="row">
				 	 
		               <div class="form-group col-sm-6">
							<label for="image" class="control-label">Import Products</label>
							<input type="file" class="form-control" id="product_info" name="product_info" required>
							<div class="help-block with-errors"><?php if(form_error('image')!=""){ echo form_error('image');} ?></div>
							<p class="name" style="display:none; color:red;"><i class="fa fa-times"></i>Please Upload Excel File..!</p>
				  	  </div>
				 	  <div class="form-group col-sm-6">
				 	 		<label for="image" class="control-label"> &nbsp;</label> <br>
				 	 		<button type="submit" onclick="return checkextension();" class="btn btn-primary" >Upload</button>
				 	 	
				      </div>
		        </div>
		        	 <div class="row">
		              <div class="form-group col-sm-6">
						<div class="row">
		                  <div class="form-group">
		                  	  <label for="image" class="control-label"> Download Excel Format</label><br>
				 		 <a href="<?php echo base_url();?>assets/example.xlsx"> Download</a> 
						
						  </div>
		                </div>
		              </div>
		             </div>
		       
            </form>
          </div>
        </div>

		<?php  if(isset($acceptedprodct)){	?>
        	<div class="col-lg-4 col-xs-12">
				<div class="alert alert-success alert-dismissable">
						<h5 >Following IMEI Number Is Successfilly uploaded. Count = <?php echo count($acceptedprodct); ?></h5>
						<?php foreach($acceptedprodct as $row)
							  {?>
							  	<h4> <?php echo $row['imei']; ?> </h4>	
                        <?php } ?>
                </div>
		
			</div>
		<?php }	?>		
		<?php  if(isset($rejectedprodct)){	?>
        	<div class="col-lg-4 col-xs-12">
				<div class="alert alert-danger alert-dismissable">
						<h5 >Following IMEI Number Already Exist. Count = <?php echo count($rejectedprodct); ?></h5>
						<?php foreach($rejectedprodct as $row)
							  {?>
							  	<h4> <?php echo $row['imei']; ?> </h4>	
                             
							<?php  } ?>
				</div>
			</div>
		<?php }	?>
		<?php  if(isset($invalidimei)){	?>	
			<div class="col-lg-4 col-xs-12">
				<div class="alert alert-info alert-dismissable">
						<h5 >Following IMEI Number Are Invalid. Count = <?php echo count($invalidimei); ?></h5>
						<?php foreach($invalidimei as $row)
							  {?>
							  	<h4> <?php echo $row['imei']; ?> </h4>	
                             
							<?php  } ?>
				</div>
			</div>
		<?php }	?>	
      </div>
      <!-- /.row -->  

         	
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/admin/users.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script type="text/javascript">
		setTimeout(function() {
        $('#timeout').fadeToggle('slow');
    }, 3000);


function checkextension(){
var filename =	document.getElementById("product_info").value;
var ext = filename.split('.').pop();

 if(ext!='xlsx' || ext!='xls' ) 
        {
            $('.name').toggle("show");
            setTimeout(function() 
                { 
                    $(".name").toggle("hide");
                }, 3000);
            return false;
        } 

return true;
}	
</script>
</body>
</html>
