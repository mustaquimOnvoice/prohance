<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Preloader --
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<!-- Preloader -->	
<div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
					<li><a href="<?php echo site_url('Admin/dashboard/');?>">Dashboard</a></li>
                    <li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Category Master<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                             <li> <a href="<?php echo site_url('Admin/add_category/');?>">Add Categroy</a> </li>
                            <li> <a href="<?php echo site_url('Admin/view_category/');?>">View Category</a> </li>
                        </ul>
                    </li>
					<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">User Master<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                             <li> <a href="<?php echo site_url('Admin/add_user/');?>">Add User</a> </li>
                            <li> <a href="<?php echo site_url('Admin/view_user/');?>">View User</a> </li>
                        </ul>
                    </li>
                   <li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">File Master<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                             <li> <a href="<?php echo site_url('Admin/add_file/');?>">Add File</a> </li>
                            <li> <a href="<?php echo site_url('Admin/view_file/');?>">View File</a> </li>
                        </ul>
                    </li>
					<li><a href="<?php echo site_url('login/logout/');?>">Logout</a></li>
                    
                    
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->