<title>Search By IMEI</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
<script>
	var tblexport = true;
	console.log('tblexport: '+tblexport);
  console.log('CSS: assets/css/common/listing.css');
</script>
<!-- Listing CSS -->
<link href="<?php echo base_url();?>assets/css/common/listing.css"   rel="stylesheet">
</head>

<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
	<div class="row">
		<div class="white-box col-sm-12">
		  <form method="post" name="date-form" id="date-form" action="<?php echo site_url('Adminreport/sales_detail_report_sess'); ?>" data-toggle="validator">       
			<div class="col-sm-2">
				<input type="text" class="form-control" id="imei" name="imei" placeholder="IMEI" value="<?php @$imei = $select['imei']; echo ($imei == '') ? '' : $imei ; ?>" required>
			</div>
			
			<div class="col-sm-2">
			  <button type="submit" name="submit" value="filter" class="pull-left btn-xs btn-success">Search</button>       
			  <button type="submit" name="submit" value="createxls" class="pull-left btn-xs btn-primary"><i class="fa fa-file-excel-o"></i> Export Excel</button>
			</div>
		  </form>
		 
		</div>
	</div>      
	<div class="row">
		<div class="col-lg-12 col-xs-12">
		<div id ="resultMsg">
		</div>
			<?php if($this->session->flashdata('success')){	?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
			<?php } if($this->session->flashdata('error')){	?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('error') ?>
				</div>
			<?php }	?>
		</div>
	</div>
      <div class="row">
		<div class="col-sm-12">
          <div class="white-box">
            <div class="table-responsive">
                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Level Type</th>
                            <th>From Code</th>
                            <th>From Firmname</th>
                            <th>To Code</th>
                            <th>To Firmname</th>
                            <th>Date</th>
                            <th>IMEI</th>
                            <th>Item Code</th>
                        </tr>
                    </thead>
                 
                    <tbody>
                         
                        <?php $i = 1;
                       // print_r($results);
                        if(!empty($results)){
								foreach($results as $row){
									if($row['level_type']=='1' || $row['level_type']=='2' ||$row['level_type']=='3' ||$row['level_type']=='4'){	
								 ?>
										<tr>
											<td><?php echo $i++;?></td>
											<td><?php echo "Intermediary";?></td>
											<td><?php echo "Anuron";?></td>
											<td><?php echo "Anuron";?></td>
											<td><?php echo $row['nd_code'];?></td>
											<td><?php echo $row['nd_firmname'];?></td>
											<td><?php echo date("d-M-y", strtotime($row['nd_date']));?></td>
											<td><?php echo $row['imei'];?></td>
											<td><?php echo $row['item_code'];?></td>								
										</tr>
									<?php } if($row['level_type']=='2' ||$row['level_type']=='3' ||$row['level_type']=='4'){ ?>
												<tr>
													<td><?php echo $i++;?></td>
													<td><?php echo "Primary";?></td>
													<td><?php echo $row['nd_code'];?></td>
													<td><?php echo $row['nd_firmname'];?></td>
													<td><?php echo $row['d_code'];?></td>
													<td><?php echo $row['d_firmname'];?></td>
													<td><?php echo date("d-M-y", strtotime($row['d_date']));?></td>
													<td><?php echo $row['imei'];?></td>
													<td><?php echo $row['item_code'];?></td>								
												</tr>
									<?php } if($row['level_type']=='3'||$row['level_type']=='4'){ ?>
												<tr>
													<td><?php echo $i++;?></td>
													<td><?php echo "Secondary";?></td>
													<td><?php echo $row['d_code'];?></td>
													<td><?php echo $row['d_firmname'];?></td>
													<td><?php echo $row['rt_code'];?></td>
													<td><?php echo $row['rt_firmname'];?></td>
													<td><?php echo date("d-M-y", strtotime($row['rt_date']));?></td>
													<td><?php echo $row['imei'];?></td>
													<td><?php echo $row['item_code'];?></td>								
												</tr>
									  <?php } if($row['level_type']=='4'){?>
									  			<tr>
													<td><?php echo $i++;?></td>
													<td><?php echo "Activation";?></td>
													<td><?php echo $row['rt_code'];?></td>
													<td><?php echo $row['rt_firmname'];?></td>
													<td><?php echo $row['c_code'];?></td>
													<td><?php echo $row['c_firmname'].'('.$row['c_mno'].')';?></td>
													<td><?php echo date("d-M-y", strtotime($row['c_date']));?></td>
													<td><?php echo $row['imei'];?></td>
													<td><?php echo $row['item_code'];?></td>								
												</tr>
									  <?php }?>	
						<?php  } } ?>                    
                    </tbody>
                              
                </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/common/listing.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

</body>
</html>
