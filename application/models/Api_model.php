<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class api_model extends CI_Model {
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	
	final function check_token($tableName, $atsm_code, $device_token){
		$TableValues['device_token'] = $device_token;
		$TableValues['mr_code'] = $atsm_code;
		$TableValues['user_status'] = '2';
		$temp = $this->Base_Models->GetAllValues ( $tableName, $TableValues, array('device_token'));
		
		if(count($temp)>0){
			return true;
		}
		$response ['message'] = "fail";
		$response ['result'] =  "Token expired";
		echo json_encode($response);
		die();
	}
	
	final function check_activate_imei($tableName, $imei){
		$TableValues['imei'] = $imei;
		// $TableValues['level_type'] = '3';
		$temp = $this->Base_Models->GetSingleDetails ( $tableName, $TableValues, array('level_type'));
		switch($temp->level_type){
			case 0:
			case 1:
			case 2:
				$response ['message'] = "fail";
				$response ['result'] =  "Not in retailer stock";
				break;
			case 3:
				$response ['status'] = 'RT';
				return true;
			case 4:
				$response ['message'] = "fail";
				$response ['result'] =  "IMEI Already Activated";
				break;
		}
		echo json_encode($response);
		die();
	}
	
	final function check_attendance($tableName, $atsm_code, $attnd_date){
		$TableValues['atsm_code'] = $atsm_code;
		// $TableValues['inorout'] = $inorout;
		$TableValues['attendance_date'] = $attnd_date;
		$temp = $this->Base_Models->GetAllValues ( $tableName, $TableValues, array('attendanceId','inorout','weekoff'));
		if(count($temp)>0){
			$response ['message'] = "done";
			// $response ['result_code'] = "false";
			// $response ['result'] =  "Attendance already exist";
			$response ['am'] = 'false';
			$response ['pm'] = 'false';
			
			//inorout
			if(@$temp[0]['inorout'] == '0'){
				$response ['am'] = 'true';
			}
			if(@$temp[1]['inorout'] == '1'){
				$response ['pm'] = 'true';
			}
			//weekoff
			if(@$temp[0]['weekoff'] == '0'){
				$response ['weekoff'] = 'false';
			}else{
				$response ['weekoff'] = 'true';
			}
			
		}else{
			$response ['message'] = "done";
			// $response ['result_code'] = "true";
			$response ['result'] =  "Attendance not found";
		}
		echo json_encode($response);
		die();
	}
	
	final function check_tsm_attendance($tableName, $atsm_code, $attnd_date){
		$TableValues['atsm_code'] = $atsm_code;
		// $TableValues['inorout'] = $inorout;
		$TableValues['attendance_date'] = $attnd_date;
		$temp = $this->Base_Models->GetAllValues ( $tableName, $TableValues, array('attendanceId','inorout','weekoff','attendance_time'));
		
		$response ['pending'] = 'true';
		$response ['am'] = 'false';
		$response ['am_time'] = '';
		$response ['pm'] = 'false';
		$response ['pm_time'] = '';
		$response ['weekoff'] = 'false';
		if(count($temp)>0){			
			//inorout
			if(@$temp[0]['inorout'] == '0'){
				$response ['pending'] = 'false';
				$response ['am'] = 'true';
				$response ['am_time'] = $temp[0]['attendance_time'];
			}
			if(@$temp[1]['inorout'] == '1'){
				$response ['pm'] = 'true';
				$response ['pm_time'] = $temp[0]['attendance_time'];
			}
			//weekoff
			if(@$temp[0]['weekoff'] == '0'){
				$response ['weekoff'] = 'false';
				$response ['pending'] = 'false';
			}else{
				$response ['weekoff'] = 'true';
				$response ['pending'] = 'false';
			}			
		}
		return $response;
	}
    
	//count imei activation of retailer under TSM
	final function imei_activation_count($atsm_id){
		$res = $this->db->query("SELECT rt.rt_id, rt.rt_code, rt.firmname, Group_concat(tstrt.imei SEPARATOR ',') as imei, COUNT(tstrt.imei) AS count_imei
						FROM retailer as rt 
						LEFT JOIN tbl_sales_to_rt as tstrt 
						ON rt.rt_id = tstrt.rt_id
						WHERE rt.tsm_id = $atsm_id
						AND tstrt.item_status = '1'
						GROUP BY rt.rt_id")->result_array();
		return $res;
	}
}
?>