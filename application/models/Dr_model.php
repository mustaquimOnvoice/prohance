<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class dr_model extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	
	//dr list
	public function dr_list_sess($fdate,$todate, $limit = null, $start = null, $count= false){
		if( $count == true ){
			$select = array('doc_id');
		}else{
			$select = array('doc_id','name','mobile_no','email','specialty','hospital','language','mr_id','mr_code','created_at','(select image_url from images where ref_code = doc.doc_id AND type = "1" Order by id desc limit 1) as pro_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id asc limit 1 ) as frame_pic2');					
		}
		$this->db->select($select,FALSE)
				->from('doc')
				->where(array('frame_status'=> "1"))
				->where(array('created_at  >='=> "$fdate"))
				->where(array('created_at  <='=> "$todate"))
				->order_by("doc_id");
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		
		if( $count == true ){
			return $res->num_rows();
		}else{
			return $res->result_array();
		}
	}

	//dr list
	public function dr_list_search($fdate,$todate, $limit = null, $start = null, $count= false, $dr_name=null){
		if( $count == true ){
			$select = array('doc_id');
		}else{
			$select = array('doc_id','name','mobile_no','email','specialty','hospital','language','mr_id','mr_code','created_at','(select image_url from images where ref_code = doc.doc_id AND type = "1" Order by id desc limit 1) as pro_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id asc limit 1 ) as frame_pic2');					
		}
		$this->db->select($select,FALSE)
				->from('doc')
				->where(array('frame_status'=> "1"))
				->where(array('created_at  >='=> "$fdate"))
				->where(array('created_at  <='=> "$todate"));
		if(!empty($dr_name)){
			$this->db->like('name', "$dr_name");
			$this->db->or_like('mobile_no', "$dr_name");
			$this->db->or_like('email', "$dr_name");
		}
		$this->db->order_by("doc_id");
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		
		if( $count == true ){
			return $res->num_rows();
		}else{
			return $res->result_array();
		}
	}

	//dr pending list
	public function dr_list_pending_sess($fdate,$todate, $limit = null, $start = null, $count= false){
		if( $count == true ){
			$select = array('doc_id');
		}else{
			$select = array('doc_id','name','mobile_no','email','specialty','hospital','language','mr_id','mr_code','created_at','(select image_url from images where ref_code = doc.doc_id AND type = "1" Order by id desc limit 1) as pro_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id asc limit 1 ) as frame_pic2');					
		}
		$this->db->select($select,FALSE)
				->from('doc')
				->where(array('frame_status'=> "0"))
				->where(array('created_at  >='=> "$fdate"))
				->where(array('created_at  <='=> "$todate"))
				->order_by("doc_id");
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		
		if( $count == true ){
			return $res->num_rows();
		}else{
			return $res->result_array();
		}
	}

	//dr list
	public function dr_list_pending_search($fdate,$todate, $limit = null, $start = null, $count= false, $dr_name=null){
		if( $count == true ){
			$select = array('doc_id');
		}else{
			$select = array('doc_id','name','mobile_no','email','specialty','hospital','language','mr_id','mr_code','created_at','(select image_url from images where ref_code = doc.doc_id AND type = "1" Order by id desc limit 1) as pro_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id desc limit 1) as frame_pic','(select image_url from images where ref_code = doc.doc_id AND type = "3" Order by id asc limit 1 ) as frame_pic2');					
		}
		$this->db->select($select,FALSE)
				->from('doc')
				->where(array('frame_status'=> "0"))
				->where(array('created_at  >='=> "$fdate"))
				->where(array('created_at  <='=> "$todate"));
		if(!empty($dr_name)){
			$this->db->like('name', "$dr_name");
			$this->db->or_like('mobile_no', "$dr_name");
			$this->db->or_like('email', "$dr_name");
		}
		$this->db->order_by("doc_id");
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		
		if( $count == true ){
			return $res->num_rows();
		}else{
			return $res->result_array();
		}
	}

	public function fetch_frame	($mr_id,$theme_id,$limit = null){
		$select = array('d.doc_id', 'd.name', 'd.mobile_no', 'd.email', 'd.specialty', 'd.hospital', '(select code from languages where code = i.language order by id limit 1) as language_code', '(select name from languages where code = i.language order by id limit 1) as language_name', 'd.mr_id', 'd.created_at', 'i.image_url as image','i.theme_id');
		$where = array('d.mr_id' => $mr_id,'i.type' => '3','i.theme_id' => $theme_id );
		$this->db->select($select)
				->from('doc as d')
				->join('images as i','i.ref_code = d.doc_id','right')
				->where($where)
				->order_by('d.doc_id','DESC');
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		return $res->result_array();

	}

	public function fetch_allframe($doc_id){
		$select = array('i.ref_code', 'i.theme_id' ,'t.theme_name','GROUP_CONCAT(i.language) as language', 'GROUP_CONCAT(i.image_url) as image_url', 'GROUP_CONCAT(i.name) as imagename','GROUP_CONCAT(i.description) as description');
		$where = array('i.type' => '3','i.status' => '1','i.ref_code' => $doc_id);
		$this->db->select($select)
				->from('images as i')
				->join('themes as t','t.theme_id = i.theme_id','left')
				->where($where)
				->order_by('i.ref_code','DESC');
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		return $res->result_array();

	}

	public function fetch_empty_frame($mr_id,$limit = null){
		$select = array('d.doc_id', 'd.name', 'd.mobile_no', 'd.email', 'd.specialty', 'd.hospital', '(select code from languages where code = d.language order by id limit 1) as language_code', '(select name from languages where code = d.language order by id limit 1) as language_name', 'd.mr_id', 'd.created_at', '(select image_url from images where type = "1" AND ref_code = d.doc_id order by id limit 1) as dr_image', ' "" as image');
		$where = array('d.mr_id' => $mr_id,'d.frame_status' => '0');
		$this->db->select($select)
				->from('doc as d')
				->where($where)
				->order_by('d.doc_id','DESC');
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		return $res->result_array();

	}

	// public function fetch_empty_frame($mr_id,$limit = null){
		// $select = array('d.doc_id', 'd.name', 'd.mobile_no', 'd.email', 'd.specialty', 'd.hospital', '(select code from languages where code = d.language order by id limit 1) as language_code', '(select name from languages where code = d.language order by id limit 1) as language_name', 'd.mr_id', 'd.created_at', '(select image_url from images where type = "1" AND ref_code = d.doc_id order by id limit 1) as dr_image','(select image_url from images where type = "3" AND ref_code = d.doc_id order by id limit 1) as image');
		// $where = array('d.mr_id' => $mr_id);

		// $this->db->select($select)
				// ->from('doc as d')
				// ->where($where)
				// ->order_by('d.doc_id','DESC');
		// if(isset ( $limit )){
			// $this->db->limit( $limit, $start );
		// }
		// $res = $this->db->get();
		// return $res->result_array();

	// }
}
?>