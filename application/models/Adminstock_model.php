<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class adminstock_model extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	
	//get all users stock count
	public function get_all_stock_count($where = null){
		$this->db->select('imei')
				->from('tbl_item_sales')
				->where(array('level_type !='=> '4'));
			if (isset ( $imei )){
				$this->db->where(array('imei'=>$imei));
			}
		$res = $this->db->get();
		return	$res->num_rows();
	}
	
	//get all users stock
	public function get_all_stock($imei = null, $limit = null, $start = null){
		$select = "tis.item_code, tis.imei, tis.level_type, 
					(CASE
					 WHEN tis.level_type = '0' THEN 'Anuron'
					 WHEN tis.level_type = '1' THEN (select nd_code from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
					 WHEN tis.level_type = '2' THEN (select d_code from distributor where d_id = tis.d_id Order by nd_id desc limit 1)
					 WHEN tis.level_type = '3' THEN (select rt_code from retailer where rt_id = tis.rt_id Order by nd_id desc limit 1)
					 ELSE 0
					END) as code,
					(CASE
					 WHEN tis.level_type = '0' THEN 'Anuron'
					 WHEN tis.level_type = '1' THEN (select firmname from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
					 WHEN tis.level_type = '2' THEN (select firmname from distributor where d_id = tis.d_id Order by d_id desc limit 1)
					 WHEN tis.level_type = '3' THEN (select firmname from retailer where rt_id = tis.rt_id Order by rt_id desc limit 1)
					 ELSE 0
					END) as firmname,
					(CASE
					 WHEN tis.level_type = '0' THEN 'Anuron'
					 WHEN tis.level_type = '1' THEN 'National Distributor'
					 WHEN tis.level_type = '2' THEN 'Distributor'
					 WHEN tis.level_type = '3' THEN 'Retailer'
					 ELSE 0
					END) as level,
                    (CASE
					 WHEN tis.level_type = '0' THEN (select tis.inserted_on)
					 WHEN tis.level_type = '1' THEN (select tis.nd_date)
					 WHEN tis.level_type = '2' THEN (select tis.d_date)
					 WHEN tis.level_type = '3' THEN (select tis.rt_date)
					 ELSE 0
					END) as stock_date
					";
		$this->db->select($select,FALSE)
				->from('tbl_item_sales as tis')
				->where(array('level_type !='=> '4'));
		if(isset ( $imei )){
			$this->db->where(array('tis.imei'=> $imei));
		}
			$this->db->order_by("tis.level_type");
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		return $res->result_array();
	}
}
?>