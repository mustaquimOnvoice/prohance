<?php 

function getCityName($id) {
   $CI = get_instance();
   $CI->load->model('base_models');    
   $res = $CI->base_models->get_records('tbl_city',array('name'),array('id'=>$id),true);
   return $res['name'];
}

function getCityRates($id) {
   $CI = get_instance();
   $CI->load->model('base_models');    
   $res = $CI->base_models->get_records('tbl_booking',array('city_rate'),array('adv_id'=>$id));
   return $res;
}

function getTodaysBooking($time,$cityid,$date) {
   $CI = get_instance();
   $CI->load->model('base_models');    
   $res = $CI->base_models->getTodaysBooking($time,$cityid,$date);
   return $res;
}

function getlanguageName($code) {
   $CI = get_instance();
   $CI->load->model('base_models');    
   $res = $CI->base_models->getlanguageName('languages',array('name'),array('code'=>$code));
   return $res;
}
?>